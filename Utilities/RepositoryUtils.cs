﻿using System;
using System.Collections.Generic;
using System.Linq;
using GFS.BackendTools;
using UniRx;

namespace GFS.Utilities
{
    public interface IOwnedData
    {
        string OwnerId { get; }
    }

    public static class RepositoryUtils
    {
        public static IObservable<IEnumerable<T>> GetUserDataStream<T>(this IRepository<T> repository, string playerId) where T : IOwnedData
        {
            IObservable<Unit> triggerStream = new Subject<Unit>();
            foreach (var t in repository.Keys.Value
                    .Where(key => repository.GetData(key).Value.OwnerId == playerId)
                    .Select(key => repository.GetData(key)))
            {
                triggerStream.CombineLatest(t, (_, __) => Unit.Default);
            }

            return triggerStream
                .Select(_ => repository.GetUserData(playerId));
        }


        public static IEnumerable<T> GetUserData<T>(this IRepository<T> repository, string playerId) where T : IOwnedData
        {
            return repository.Keys.Value
                .Where(key => repository.GetData(key).Value.OwnerId == playerId)
                .Select(key => repository.GetData(key).Value);
        }
    }
}