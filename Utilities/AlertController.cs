﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using Zenject;
using PaperPlaneTools;

namespace GFS.BackendTools
{
    public struct AlertParameters
    {
        public string text;
        public string subtext;
        public AlertButton[] buttons;
    }

    public struct AlertButton
    {
        public string text;
        public Action action;
    }

    public interface IAlertController
    {
        void ShowAlert(AlertParameters param);
    }

    public class AlertController : IAlertController
    {
        public void ShowAlert(AlertParameters param)
        {
#if UNITY_EDITOR
            param.buttons.First(button => button.action != null).action();
#else
            var alert = new Alert(param.text, param.subtext);

            foreach (var b in param.buttons)
            {
                alert.SetNeutralButton(b.text, b.action);
            }

            alert.Show();
#endif
        }
    }
}