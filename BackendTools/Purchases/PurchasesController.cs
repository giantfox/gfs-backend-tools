﻿using System;
using System.Collections.Generic;
using System.Linq;
using GFS.Utilities;
using PlayFab;
using PlayFab.ClientModels;
using UniRx;
using UnityEngine;
using UnityEngine.Purchasing;
using Zenject;
using GFS.Backend;

namespace GFS.BackendTools
{
    [Serializable]
    public class StoreItemData
    {
        public string id;
        public bool isPurchased;
        public SubscriptionData subscriptionData;
        public List<string> bundleItemIDs;
    }

    [Serializable]
    public struct SubscriptionData
    {
        public bool isSubscription;
        public string expirationData;
    }

    public interface IPurchasesController
    {
        IReadOnlyReactiveProperty<string[]> ItemIds { get; }
        IReactiveProperty<string> PurchasedItemId { get; set; } //Set to recently purchased item
        StoreItemData GetItemData(string itemId);

        void PurchaseItem(string itemId);

        void TestRefresh ();

        void RegisterForPurchaseFail ( System.Action<Product, PurchaseFailureReason> purchaseFailData );
        void UnregisterForPurchaseFail ( System.Action<Product, PurchaseFailureReason> purchaseFailData );

        void RestorePurchases ( Action<bool> purchasesRestored );

        bool IsPlayerElegibleForPromotion ();
        string GetPromotionItemID ();

        void GetDataFromPlayfabCatalog (System.Action success, System.Action<string> errorAction);

        List<StoreItemData> GetStoreItems ();
    }

    public interface IPurchasesStrategy
    {
        AppStore Store { get; }
        IReadOnlyReactiveProperty<bool> IsInitialized { get; }
        Product[] Products { get; }

        SubscriptionData GetItemSubscriptionData(string itemId);
        IObservable<bool> PurchaseItem(string itemId);

        event System.Action<Product, PurchaseFailureReason> purchaseFailDelegate;

        void RestorePurchases( Action<bool> purchasesRestored );
    }

    public class PurchasesController : ReadOnlyBackendCache, IPurchasesController
    {
        [Inject] DiContainer container;
        [Inject] IAlertController alertController;

        private IPurchasesStrategy strategy;
        //private Dictionary<string, IReadOnlyReactiveProperty<StoreItemData>> propertyCache = new Dictionary<string, IReadOnlyReactiveProperty<StoreItemData>>();
        private List<StoreItemData> storeItems = new List<StoreItemData> ();

        protected override string DiskKey => "PurchaseData";
        public IReadOnlyReactiveProperty<string[]> ItemIds => keys;
        public IReactiveProperty<string> PurchasedItemId { get; set; }
        public void PurchaseItem (string itemId)
        {
            PurchasedItemId.Value = "";
            strategy.PurchaseItem (itemId)
                .First ()
                .Subscribe (_ =>
                 {
                     if ( itemId.ToLower ().Contains ("subscription") )
                     {
                         PlayerPrefs.SetString ("lastPUrchasedItem", "subscription");

                         PlayerPrefs.SetInt ("playerGotFredFamilySubscriptionBefore", 1);
                     }
                     else
                     {
                         PlayerPrefs.SetString ("lastPUrchasedItem", "unlock");

                         PlayerPrefs.SetInt ("playerGotUnlockGame", 1);
                     }

                     Debug.Log ("purchase successL refresh cache");

                     LaunchRefresh ();
                     PurchasedItemId.Value = itemId;

                     Debug.Log ("1- purcahased last item: " + PlayerPrefs.GetString ("lastPUrchasedItem"));
                      //RefreshCache ();
                  });
        }

        public void TestRefresh ()
        {
            LaunchRefresh ();
        }

        public override void Initialize ()
        {
            base.Initialize ();

            clientState.isLoggedIn
                .CombineLatest (clientState.isConnected, (isLoggedIn, isConnected) => isLoggedIn && isConnected)
                .CombineLatest (clientState.isConnected, (isLoggedIn, isConnected) => isLoggedIn && isConnected)
                .Where (isLoggedIn => isLoggedIn)
                .Subscribe (_ =>
                 {
                     GetDataFromPlayfabCatalog ( null, null );
                 });
        }

        public List<StoreItemData> GetStoreItems ()
        {
            return storeItems;
        }

        public void GetDataFromPlayfabCatalog (System.Action success, System.Action<string> errorAction)
        {
            if ( !PlayFabClientAPI.IsClientLoggedIn () )
            {
                errorAction?.Invoke ( "not logged in" );

                return;
            }

            PurchasedItemId = new ReactiveProperty<string> ("");
            PurchasesStrategyBase purchasesStrategy = CreateStrategy ();

            if ( strategy == null )
                strategy = purchasesStrategy;

            UpdatePlayfabSubscriptionExpirationData ((long)( DateTime.UtcNow - new DateTime (1970, 1, 1) ).TotalMilliseconds,
                (string date) => { Debug.Log ("date: " + date); });

            //if ( storeItems != null )
            //{
            //    success?.Invoke ();
            //    return;
            //}

            PlayFabClientAPI.GetCatalogItems (new GetCatalogItemsRequest (), result =>
            {
                storeItems = new List<StoreItemData> ();

                // Create a builder for IAP service
                var builder = ConfigurationBuilder.Instance (StandardPurchasingModule.Instance (purchasesStrategy.Store));

                var catalog = result.Catalog;

                // Register each item from the catalog
                foreach ( CatalogItem item in catalog )
                {
                    if ( item.Bundle != null )
                    {
                        Debug.Log (item.ItemId);

                        builder.AddProduct (item.ItemId, ProductType.Consumable);

                        StoreItemData storeItem = new StoreItemData
                        {
                            id = item.ItemId,
                            isPurchased = false,
                            bundleItemIDs = item.Bundle.BundledItems
                        };

                        storeItems.Add (storeItem);
                    }
                }

                purchasesStrategy.IsInitialized
                    .First (isIntialized => isIntialized)
                    .Subscribe (__ => clientState.storeInitialized.Value = true);

                // Trigger IAP service initialization
                UnityPurchasing.Initialize (purchasesStrategy, builder);

                PlayFabClientAPI.GetUserInventory (new GetUserInventoryRequest (),
                    results =>
                    {
                        foreach ( ItemInstance userItem in results.Inventory )
                        {
                            if ( userItem.ItemId == "DefaultCharacter" )
                            {
                                storeItems.Add (new StoreItemData
                                {
                                    id = userItem.ItemId,
                                    isPurchased = true
                                });
                            }
                            else
                            {
                                foreach ( StoreItemData storeItem in storeItems )
                                {
                                    storeItem.subscriptionData = strategy.GetItemSubscriptionData (storeItem.id);

                                    Debug.Log (storeItem.subscriptionData);

                                    foreach ( string bundleItemID in storeItem.bundleItemIDs )
                                    {
                                        if ( bundleItemID == userItem.ItemId )
                                        {
                                            Debug.Log (bundleItemID);

                                            if ( storeItem.subscriptionData.isSubscription )
                                            {
                                                DateTime subscriptionExpirationDate = DateTime.Parse (storeItem.subscriptionData.expirationData);

                                                var isMobileSubscritionActive = subscriptionExpirationDate >= DateTime.UtcNow.Date;

                                                long expirationDateInMilliseconds = (long)( subscriptionExpirationDate - new DateTime (1970, 1, 1) ).TotalMilliseconds;

                                                if ( !isMobileSubscritionActive )
                                                {
                                                    UpdatePlayfabSubscriptionExpirationData (expirationDateInMilliseconds, (string date) =>
                                                    {
                                                        DateTime playfabExpirationDate = new DateTime (1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc)
                                                            .AddMilliseconds (Convert.ToDouble (date));

                                                        bool isPlayfabSubscriptionActive = playfabExpirationDate >= DateTime.UtcNow.Date;

                                                        if ( !isPlayfabSubscriptionActive )
                                                            storeItem.isPurchased = false;
                                                    });
                                                }
                                                else
                                                {
                                                    UpdatePlayfabSubscriptionExpirationData (expirationDateInMilliseconds, (string date) => { });

                                                    storeItem.isPurchased = true;
                                                }
                                            }
                                            else
                                            {
                                                storeItem.isPurchased = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        success?.Invoke ();
                    },
                    getUserInventoryError =>
                    {
                        errorAction?.Invoke (getUserInventoryError.GenerateErrorReport ());
                    });

            }, error =>
            {
                //Debug.LogError (error.GenerateErrorReport ());
                errorAction?.Invoke (error.GenerateErrorReport ());
            });
        }

        protected override IObservable<Dictionary<string, string>> RefreshCache ()
        {
            var returned = new Subject<Dictionary<string, string>> ();
            storeItems = new List<StoreItemData> ();
            var catalogItems = new List<StoreItemData> ();

            UpdatePlayfabSubscriptionExpirationData ((long)( DateTime.UtcNow - new DateTime (1970, 1, 1) ).TotalMilliseconds,
                (string date) => { Debug.Log ("date: " + date); });

            PlayFabClientAPI.GetCatalogItems (new GetCatalogItemsRequest (), result =>
            {
                foreach ( var i in result.Catalog )
                {
                    if ( i.Bundle != null )
                    {
                        StoreItemData storeItem = new StoreItemData
                        {
                            id = i.ItemId,
                            isPurchased = false,
                            bundleItemIDs = i.Bundle.BundledItems
                        };

                        storeItems.Add (storeItem);
                    }
                }

                PlayFabClientAPI.GetUserInventory (new GetUserInventoryRequest (),
                    results =>
                    {
                        foreach ( ItemInstance userItem in results.Inventory )
                        {
                            if ( userItem.ItemId == "DefaultCharacter" )
                            {
                                storeItems.Add (new StoreItemData
                                {
                                    id = userItem.ItemId,
                                    isPurchased = true
                                });
                            }
                            else
                            {
                                foreach ( StoreItemData storeItem in storeItems )
                                {
                                    storeItem.subscriptionData = strategy.GetItemSubscriptionData (storeItem.id);

                                    Debug.Log (storeItem.subscriptionData);

                                    foreach ( string bundleItemID in storeItem.bundleItemIDs )
                                    {
                                        if ( bundleItemID == userItem.ItemId )
                                        {
                                            Debug.Log (bundleItemID);

                                            if ( storeItem.subscriptionData.isSubscription )
                                            {
                                                DateTime subscriptionExpirationDate = DateTime.Parse (storeItem.subscriptionData.expirationData);

                                                var isMobileSubscritionActive = subscriptionExpirationDate >= DateTime.UtcNow.Date;

                                                long expirationDateInMilliseconds = (long)( subscriptionExpirationDate - new DateTime (1970, 1, 1) ).TotalMilliseconds;

                                                if ( !isMobileSubscritionActive )
                                                {
                                                    UpdatePlayfabSubscriptionExpirationData (expirationDateInMilliseconds, (string date) =>
                                                    {
                                                        DateTime playfabExpirationDate = new DateTime (1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc)
                                                            .AddMilliseconds (Convert.ToDouble (date));

                                                        bool isPlayfabSubscriptionActive = playfabExpirationDate >= DateTime.UtcNow.Date;

                                                        if ( !isPlayfabSubscriptionActive )
                                                            storeItem.isPurchased = false;
                                                    });
                                                }
                                                else
                                                {
                                                    UpdatePlayfabSubscriptionExpirationData (expirationDateInMilliseconds, (string date) => { });

                                                    storeItem.isPurchased = true;
                                                }
                                            }
                                            else
                                            {
                                                storeItem.isPurchased = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        returned.OnNext (storeItems.ToDictionary (i => i.id, i => JsonUtility.ToJson (i)));

                        //foreach (var i in results.Inventory)
                        //{
                        //    var storeItem = items.FirstOrDefault(item => item.id == i.ItemId);

                        //    if (storeItem == null)
                        //    {
                        //        Debug.Log ( i.ItemId );

                        //        items.Add(new StoreItemData
                        //        {
                        //            id = i.ItemId,
                        //            isPurchased = true
                        //        });
                        //    }
                        //    else
                        //        storeItem.isPurchased = true;

                        //    storeItem.subscriptionData = strategy.GetItemSubscriptionData(storeItem.id);
                        //}

                        //returned.OnNext(items.ToDictionary(i => i.id, i => JsonUtility.ToJson(i)));
                    },
                    error => Debug.LogError (error.GenerateErrorReport ()));

            }, error => Debug.LogError (error.GenerateErrorReport ()));

            return returned;
        }

        public void RegisterForPurchaseFail ( System.Action<Product, PurchaseFailureReason> _purchaseFailData )
        {
            strategy.purchaseFailDelegate += _purchaseFailData;
            strategy.purchaseFailDelegate += purchaseFailData;
        }

        public void UnregisterForPurchaseFail ( System.Action<Product, PurchaseFailureReason> _purchaseFailData )
        {
            strategy.purchaseFailDelegate -= _purchaseFailData;
            strategy.purchaseFailDelegate -= purchaseFailData;
        }

        void purchaseFailData ( Product product, PurchaseFailureReason purchaseFailureReason )
        {
            string purchaseMessage = string.Format ( "OnPurchaseFailed: FAIL. " +
                "Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, purchaseFailureReason );

            clientState.errorMessage.Value = purchaseMessage;
        }

        public void RestorePurchases ( Action<bool> purchasesRestored )
        {
            strategy.RestorePurchases ( ( bool _purchasesRestored ) =>
            {
                RefreshCache ();
            } );
        }

        public StoreItemData GetItemData ( string itemId )
        {
            foreach ( StoreItemData storeItemData in storeItems )
            {
                if ( storeItemData.id == itemId )
                    return storeItemData;
            }

            return null;

            //if ( !storeItems.ContainsKey ( itemId ) )
            //    storeItems[ itemId ] = GetData ( itemId )
            //         .GetConverted ( json => JsonUtility.FromJson<StoreItemData> ( json ) );

            //return storeItems[ itemId ];
        }

        public bool IsPlayerElegibleForPromotion ()
        {
            string lastPurchasedItem = PlayerPrefs.GetString ( "lastPUrchasedItem", "" );

            Debug.Log ( "2- purcahased last item: " + PlayerPrefs.GetString ( "lastPUrchasedItem" ) );

            if ( lastPurchasedItem == "subscription" )
            {
                int hasUnlockGame = PlayerPrefs.GetInt ( "playerGotUnlockGame", 0 );

                if ( hasUnlockGame == 0 )
                    return true;
                else
                {
                    return false;
                }
            }
            else if ( lastPurchasedItem == "unlock" )
            {
                int hadSubscription = PlayerPrefs.GetInt ( "playerGotFredFamilySubscriptionBefore", 0 );

                if ( hadSubscription == 0 )
                    return true;
                else
                {
                    return false;
                }
            }
            else
                return false;
        }

        public string GetPromotionItemID ()
        {
            string lastPurchasedItem = PlayerPrefs.GetString ( "lastPUrchasedItem", "" );

            PlayerPrefs.SetString ( "lastPUrchasedItem", "" );

            if ( lastPurchasedItem == "subscription" )
            {
                return "unlock_50off";
            }
            else if ( lastPurchasedItem == "unlock" )
            {
                return "com.englishanyone.frederick.unlockgamesubscriptionfreemonth";
            }
            else
            {
                return "";
            }
        }

        void UpdatePlayfabSubscriptionExpirationData ( long expirationDate, Action<string> action )
        {
            Debug.Log ( "called UpdatePlayfabSubscriptionExpirationData" );

            ExecuteCloudScriptRequest executeCloudScript = new ExecuteCloudScriptRequest
            {
                FunctionName = "UpdateSubscriptionData",
                FunctionParameter = new
                {
                    EXPIRATIONDATE = expirationDate
                }
            };
            PlayFabClientAPI.ExecuteCloudScript ( executeCloudScript,
            ( ExecuteCloudScriptResult result ) =>
            {
                Debug.Log ( "Subscription Date: " + result.FunctionResult.ToString () );

                Debug.Log ( result.FunctionResult.ToString () );

                action?.Invoke ( result.FunctionResult.ToString () );
            },
            ( PlayFabError error ) =>
            {
                Debug.Log ( error.Error.ToString () );

                action?.Invoke ( "" );
            } );
        }

        private PurchasesStrategyBase CreateStrategy()
        {
            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                    return container.Instantiate<GooglePlayPurchasesStrategy>();
                case RuntimePlatform.IPhonePlayer:
                    return container.Instantiate<AppleAppStorePurchasesStrategy>();
            }
            return container.Instantiate<StubPurchaseStrategy>();
        }
    }

}