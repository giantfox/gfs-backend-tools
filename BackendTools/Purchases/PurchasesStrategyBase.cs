﻿using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using UnityEngine.Purchasing;

namespace GFS.BackendTools
{
    public abstract class PurchasesStrategyBase : IStoreListener, IPurchasesStrategy
    {
        protected IStoreController storeController;
        protected IExtensionProvider extensions;

        private Subject<Tuple<string, bool>> transactionCompleted = new Subject<Tuple<string, bool>>();

        private ReactiveProperty<bool> isInitialized = new ReactiveProperty<bool>(false);
        public IReadOnlyReactiveProperty<bool> IsInitialized => isInitialized;

        public Product[] Products { get => storeController.products.all; }

        public abstract AppStore Store { get; }
        public abstract IObservable<bool> ValidateReceipt(PurchaseEventArgs e);

        public event System.Action<Product, PurchaseFailureReason> purchaseFailDelegate;

        private bool shouldRestoreWhenInitialize = false;


        // This is invoked automatically when IAP service is initialized
        public void OnInitialized ( IStoreController storeController, IExtensionProvider extensions )
        {
            Debug.Log ( "OnInitialized" );

            isInitialized.Value = true;

            this.storeController = storeController;
            this.extensions = extensions;

            if ( shouldRestoreWhenInitialize )
            {
                RestorePurchases ( (bool b) => { } );
            }
        }

        //void SetData ( IStoreController storeController, IExtensionProvider extensions )
        //{
        //    this.storeController = storeController;
        //    this.extensions = extensions;

        //    Debug.Log ( this.storeController );
        //    Debug.Log ( this.extensions );
        //}

        public SubscriptionData GetItemSubscriptionData ( string itemId )
        {
            Debug.Log ( "-----" );
            Debug.Log ( itemId );

            bool isSubscription = itemId == "com.englishanyone.frederick.unlockgamesubscription"
                    || itemId == "com.englishanyone.frederick.unlockgamesubscriptionfreemonth";

            Debug.Log ( "antes do extensions" );

            if ( extensions != null && isSubscription )
            {
                Dictionary<string, string> introductoryInfo = extensions
                .GetExtension<IAppleExtensions> ()?.GetIntroductoryPriceDictionary ();

                Debug.Log ( introductoryInfo );

                var item = storeController?.products?.all?.FirstOrDefault ( i => i.definition.id == itemId );

                Debug.Log ( item.definition.type );

                //bool isSubscription = item.definition.type == ProductType.Subscription;
                // A hack, it wasn't working checking for subscription
                
                bool subscriptionValid = item.receipt != null;

#if UNITY_ANDROID
            subscriptionValid = ( item.receipt != null ) ? ReceiptIsValidForSubscriptionManager ( item.receipt ) : false;
#endif
                Debug.Log ( "It has subscription " + isSubscription );

                //foreach ( string key in introductoryInfo.Keys )
                //{
                //    Debug.Log ( key + ":" );
                //    Debug.Log ( introductoryInfo[ key ] );
                //}

                // In testflight/sandbox accounts item.receipt is null sometimes.
                // Probably that's why expiration date os only one day.

                if ( item != null && isSubscription && subscriptionValid )
                {
                    string intro_json = ( introductoryInfo == null || !introductoryInfo.ContainsKey ( item.definition.storeSpecificId ) ) ? null : introductoryInfo[ item.definition.storeSpecificId ];
                    SubscriptionManager subscriptionManager = new SubscriptionManager ( item, intro_json );

                    if ( subscriptionManager != null )
                    {
                        Debug.Log ( subscriptionManager.getSubscriptionInfo () );

                        var info = subscriptionManager.getSubscriptionInfo ();

                        Debug.Log ( info.getPurchaseDate () );
                        Debug.Log ( info.getCancelDate () );
                        Debug.Log ( info.getRemainingTime () );
                        Debug.Log ( info.isFreeTrial () );

                        return new SubscriptionData
                        {
                            isSubscription = true,
                            expirationData = info.getExpireDate ().ToString ( "yyyy-MM-dd" )
                        };
                    }
                    else
                    {
                        Debug.Log ( "Subscription Manager is not active" );

                        // probably means it is expired
                        return new SubscriptionData
                        {
                            isSubscription = true,
                            expirationData = new DateTime ( 1990, 01, 01 ).ToString ( "yyyy-MM-dd" )
                        };
                    }
                }
                else if ( isSubscription && !subscriptionValid )
                {
                    Debug.Log ( "It's a subscription but it isn't valid" );

                    // probably means it is expired
                    return new SubscriptionData
                    {
                        isSubscription = true,
                        expirationData = new DateTime ( 1990, 01, 01 ).ToString ( "yyyy-MM-dd" )
                    };
                }
            }

            Debug.Log ( "Default subscription value" );

            return new SubscriptionData ();
        }

        public void RestorePurchases ( Action<bool> purchasesRestored )
        {
            Debug.Log ( "Restore Purchases?" );

            if ( this.extensions == null )
            {
                shouldRestoreWhenInitialize = true;
            }
            else
            {
                this.extensions?.GetExtension<IAppleExtensions> ()?.RestoreTransactions ( result =>
                {
                    if ( result )
                    {
                        // This does not mean anything was restored,
                        // merely that the restoration process succeeded.
                        Debug.Log ( "Restoration worked" );
                    }
                    else
                    {
                        // Restoration failed.
                        Debug.Log ( "Restoration failed" );
                    }

                    purchasesRestored?.Invoke ( result );
                } );
            }
        }

        // This is automatically invoked automatically when IAP service failed to initialized
        public void OnInitializeFailed(InitializationFailureReason error)
        {
            Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
        }

        // This is automatically invoked automatically when purchase failed
        public void OnPurchaseFailed ( Product product, PurchaseFailureReason failureReason )
        {
            string purchaseMessage = string.Format ( "OnPurchaseFailed: FAIL. " +
                "Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason );

            Debug.Log( purchaseMessage );

            purchaseFailDelegate?.Invoke ( product, failureReason );
        }

        // This is invoked automatically when successful purchase is ready to be processed
        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
        {
            var itemId = e.purchasedProduct.definition.id;
            PurchaseProcessingResult returned;

            // Test edge case where product is unknown
            if (e.purchasedProduct == null)
            {
                Debug.LogError("Attempted to process purchase with unknown product. Ignoring");
                returned = PurchaseProcessingResult.Complete;
                // Test edge case where purchase has no receipt
            }
            else if (string.IsNullOrEmpty(e.purchasedProduct.receipt))
            {
                Debug.LogError("Attempted to process purchase with no receipt: ignoring");
                returned = PurchaseProcessingResult.Complete;
            }
            else
            {
                Debug.Log("Processing transaction: " + e.purchasedProduct.transactionID);

                ValidateReceipt(e)
                    .First()
                    .Subscribe(isValid =>
                    {
                        Debug.Log("Receipt is valid: " + isValid);

                        transactionCompleted.OnNext(Tuple.Create(itemId, true));

                        if ( !shouldRestoreWhenInitialize )
                            storeController.ConfirmPendingPurchase(e.purchasedProduct);
                    });

                returned = PurchaseProcessingResult.Pending;
            }

            if (returned == PurchaseProcessingResult.Complete)
                transactionCompleted.OnNext(Tuple.Create(itemId, false));

            return returned;
        }

        public IObservable<bool> PurchaseItem(string id)
        {
            var returned = new Subject<bool>();

            transactionCompleted
                .First(tuple => tuple.Item1 == id)
                .Subscribe(tuple => returned.OnNext(tuple.Item2));

            storeController.InitiatePurchase(id);

            return returned;
        }

        private bool ReceiptIsValidForSubscriptionManager(string receipt)
        {
            var receipt_wrapper = (Dictionary<string, object>)MiniJson.JsonDecode(receipt);
            if (!receipt_wrapper.ContainsKey("Store") || !receipt_wrapper.ContainsKey("Payload"))
            {
                Debug.Log("The product receipt does not contain enough information");
                return false;
            }
            var store = (string)receipt_wrapper["Store"];
            var payload = (string)receipt_wrapper["Payload"];

            if (payload != null)
            {
                switch (store)
                {
                    case GooglePlay.Name:
                        {
                            var payload_wrapper = (Dictionary<string, object>)MiniJson.JsonDecode(payload);
                            if (!payload_wrapper.ContainsKey("json"))
                            {
                                Debug.Log("The product receipt does not contain enough information, the 'json' field is missing");
                                return false;
                            }
                            var original_json_payload_wrapper = (Dictionary<string, object>)MiniJson.JsonDecode((string)payload_wrapper["json"]);
                            if (original_json_payload_wrapper == null || !original_json_payload_wrapper.ContainsKey("developerPayload"))
                            {
                                Debug.Log("The product receipt does not contain enough information, the 'developerPayload' field is missing");
                                return false;
                            }
                            var developerPayloadJSON = (string)original_json_payload_wrapper["developerPayload"];
                            var developerPayload_wrapper = (Dictionary<string, object>)MiniJson.JsonDecode(developerPayloadJSON);
                            if (developerPayload_wrapper == null || !developerPayload_wrapper.ContainsKey("is_free_trial") || !developerPayload_wrapper.ContainsKey("has_introductory_price_trial"))
                            {
                                Debug.Log("The product receipt does not contain enough information, the product is not purchased using 1.19 or later");
                                return false;
                            }
                            return true;
                        }
                    case AppleAppStore.Name:
                        return true;
                    case AmazonApps.Name:
                    case MacAppStore.Name:
                        {
                            return true;
                        }
                    default:
                        {
                            return false;
                        }
                }
            }
            return false;
        }
    }

    public class StubPurchaseStrategy : PurchasesStrategyBase
    {
        public override AppStore Store => throw new NotImplementedException();

        public override IObservable<bool> ValidateReceipt(PurchaseEventArgs e) => throw new NotImplementedException();
    }
}