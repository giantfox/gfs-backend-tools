﻿using System;
using PlayFab;
using PlayFab.ClientModels;
using UniRx;
using UnityEngine;
using UnityEngine.Purchasing;

namespace GFS.BackendTools
{
    public class GooglePlayPurchasesStrategy : PurchasesStrategyBase
    {
        public override AppStore Store => AppStore.GooglePlay;

        public override IObservable<bool> ValidateReceipt(PurchaseEventArgs e)
        {
            var returned = new Subject<bool>();

            // Deserialize receipt
            var googleReceipt = GooglePurchase.FromJson(e.purchasedProduct.receipt);

            // Invoke receipt validation
            // This will not only validate a receipt, but will also grant player corresponding items
            // only if receipt is valid.
            PlayFabClientAPI.ValidateGooglePlayPurchase(new ValidateGooglePlayPurchaseRequest()
            {
                // Pass in currency code in ISO format
                CurrencyCode = e.purchasedProduct.metadata.isoCurrencyCode,
                // Convert and set Purchase price
                PurchasePrice = (uint)(e.purchasedProduct.metadata.localizedPrice * 100),
                // Pass in the receipt
                ReceiptJson = googleReceipt.payloadData.json,
                // Pass in the signature
                Signature = googleReceipt.payloadData.signature
            }, result =>
            {
                returned.OnNext(true);
            },
               error =>
               {
                   Debug.LogError(error.GenerateErrorReport());
                   returned.OnNext(false);
               }
            );

            return returned;
        }

        public class GooglePurchase
        {
            public PayloadData payloadData;

            // JSON Fields, ! Case-sensitive
            public string Store;
            public string TransactionID;
            public string Payload;

            public static GooglePurchase FromJson(string json)
            {
                var purchase = JsonUtility.FromJson<GooglePurchase>(json);
                purchase.payloadData = PayloadData.FromJson(purchase.Payload);
                return purchase;
            }

            public class PayloadData
            {
                public JsonData JsonData;

                // JSON Fields, ! Case-sensitive
                public string signature;
                public string json;

                public static PayloadData FromJson(string json)
                {
                    var payload = JsonUtility.FromJson<PayloadData>(json);
                    payload.JsonData = JsonUtility.FromJson<JsonData>(payload.json);
                    return payload;
                }
            }

            // The following classes are used to deserialize JSON results provided by IAP Service
            // Please, note that JSON fields are case-sensitive and should remain fields to support Unity Deserialization via JsonUtilities
            public class JsonData
            {
                // JSON Fields, ! Case-sensitive

                public string orderId;
                public string packageName;
                public string productId;
                public long purchaseTime;
                public int purchaseState;
                public string purchaseToken;
            }
        }
    }
}