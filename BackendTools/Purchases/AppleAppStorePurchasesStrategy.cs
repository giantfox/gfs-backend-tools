﻿using System;
using System.Collections.Generic;
using PlayFab;
using PlayFab.ClientModels;
using SimpleJSON;
using UniRx;
using UnityEngine;
using UnityEngine.Purchasing;

namespace GFS.BackendTools
{
    public class AppleAppStorePurchasesStrategy : PurchasesStrategyBase
    {
        public override AppStore Store => AppStore.AppleAppStore;

        public override IObservable<bool> ValidateReceipt(PurchaseEventArgs e)
        {
            var returned = new Subject<bool>();

            var receiptPayload = JSON.Parse(e.purchasedProduct.receipt)["Payload"].Value;

            // Invoke receipt validation
            // This will not only validate a receipt, but will also grant player corresponding items
            // only if receipt is valid.
            PlayFabClientAPI.ValidateIOSReceipt(new ValidateIOSReceiptRequest()
            {
                // Pass in currency code in ISO format
                CurrencyCode = e.purchasedProduct.metadata.isoCurrencyCode,
                // Convert and set Purchase price
                PurchasePrice = (int)(e.purchasedProduct.metadata.localizedPrice * 100),
                ReceiptData = receiptPayload
            },
                result => returned.OnNext(true),
                error =>
                {
                    Debug.LogError(error.GenerateErrorReport());
                    returned.OnNext(false);
                }
            );

            return returned;
        }
    }
}