﻿using System;
using System.Collections.Generic;
using System.Linq;
using GFS.Utilities;
using PlayFab;
using PlayFab.ClientModels;
using UniRx;
using UnityEngine;
using Zenject;
using GFS.Backend;

namespace GFS.BackendTools
{
    //
    public interface IDeviceAuthenticationStrategy
    {
        IObservable<bool> LinkDevice();
        IObservable<bool> UnlinkDevice();
        IObservable<bool> LoginWithDevice(bool createAccount);
    }

    /// <summary>
    /// This handles logging into three services:
    ///     - Playfab which handles tracking player's data and serves as a frictionless way to authenticate the user
    ///     - Facebook which is used for all social interactions via the friends list
    ///     - Player.IO which is used as a database cloud service.
    /// </summary>
    public class AuthenticationController : IInitializable
    {
        [System.Serializable]
        public class Config
        {
            public bool autoLogin = true;
            public string debugOverrideDeviceId;
        }

        [Inject] ClientState clientState;
        [Inject] IAccountInfoController accountInfoController;

        IDeviceAuthenticationStrategy deviceAuthenticationStrategy = null;

        Config config;

        public string deviceId;

        public AuthenticationController(Config config)
        {
            this.config = config;

            deviceId = string.IsNullOrEmpty(config.debugOverrideDeviceId) ? SystemInfo.deviceUniqueIdentifier : config.debugOverrideDeviceId;

            deviceAuthenticationStrategy = Application.platform == RuntimePlatform.Android ? (IDeviceAuthenticationStrategy)new AndroidDeviceAuthenticationStrategy(deviceId) : new IOSDeviceAuthenticationStrategy(deviceId);
        }

        public void Initialize()
        {
            //accountInfoController.AccountInfo
            //    .Subscribe(playerInfoPayload =>
            //    {
            //        clientState.hasLocalUser.Value = accountInfoController.HasAccountInfoCached;
            //        clientState.playerId.Value = playerInfoPayload?.AccountInfo?.PlayFabId;
            //        BackendController.instance.emailLinked = !string.IsNullOrEmpty(playerInfoPayload?.AccountInfo?.PrivateInfo?.Email);
            //        BackendController.instance.emailVerified = playerInfoPayload?.PlayerProfile?.ContactEmailAddresses?.FirstOrDefault(e => e.VerificationStatus == EmailVerificationStatus.Confirmed) != null;

            //        Debug.Log ( "logged" );
            //    });

            //clientState.isConnected
            //    .First(isConnected => isConnected)
            //    .Subscribe(_ =>
            //    {
            //        if (config.autoLogin)
            //            LoginAsGuest ( () => { } );
            //        else
            //            LoginWithDevice ( false, () => { } );
            //    });

            //clientState.hasLocalUser.Value = accountInfoController.HasAccountInfoCached;
        }

        public void LoginWithEmail ( string email, string password, Action<bool, string> action )
        {
            string username = DefineUsername ( email );

            clientState.isLocked.Value = true;
            PlayFabClientAPI.LoginWithEmailAddress ( new LoginWithEmailAddressRequest
            {
                Email = email,
                Password = password
            }, results =>
            {
                clientState.errorMessage.Value = null;
                LinkDevice ()
                    .First ()
                    .Subscribe ( _ =>
                    {
                        clientState.isLoggedIn.Value = true;
                        clientState.isEmailLinked.Value = true;
                        clientState.isLocked.Value = false;

                        accountInfoController.RefreshAccountInfo ();

                        action?.Invoke ( true, "" );
                    } );
            }, error =>
            {
                clientState.isLocked.Value = false;

                action?.Invoke ( false, error.GenerateErrorReport () );
            } );
        }

        public void LinkAnonymousAccountToEmail ( string email, string password, Action<bool, string> action )
        {
            string username = DefineUsername ( email );

            clientState.isLocked.Value = true;
            if ( BackendController.instance.hasUserLoggedIn )
            {
                if ( !BackendController.instance.emailLinked )
                {
                    PlayFabClientAPI.AddUsernamePassword ( new AddUsernamePasswordRequest
                    {
                        Email = email,
                        Password = password,
                        Username = username
                    }, results =>
                    {
                        clientState.errorMessage.Value = null;
                        clientState.isEmailLinked.Value = true;
                        clientState.isLocked.Value = false;

                        AddContactEmail ( email );

                        action?.Invoke ( true, "" );

                    }, error =>
                    {
                        clientState.isLocked.Value = false;

                        action?.Invoke ( false, error.GenerateErrorReport () );
                    } );
                }
            }
        }

        string DefineUsername ( string email )
        {
            string username = email?.RemoveAllNonAlphaNumeric ();

            if ( username.Length > 20 )
                username = email?.RemoveAllNonAlphaNumeric ().Substring ( 0,
                    email.RemoveAllNonAlphaNumeric ().Length >= 20 ? 20 : email.RemoveAllNonAlphaNumeric ().Length );

            return username;
        }

        public void RegisterWithEmail ( string email, string password, Action accountCreationSuccess )
        {
            clientState.isLocked.Value = true;
            PlayFabClientAPI.RegisterPlayFabUser(new RegisterPlayFabUserRequest
            {
                Email = email,
                Password = password,
                RequireBothUsernameAndEmail = false
            }, results =>
            {
                LinkDevice()
                    .First()
                    .Subscribe(_ =>
                    {
                        clientState.isLocked.Value = false;
                        accountInfoController.RefreshAccountInfo();

                        accountCreationSuccess?.Invoke ();
                    } );

                AddContactEmail(email);
            }, error =>
            {
                Debug.LogError ( error.ErrorMessage );
                clientState.errorMessage.Value = error.GenerateUserFriendlyReport();
                clientState.isLocked.Value = false;
            });
        }

        public void Logout()
        {
            clientState.isLocked.Value = true;
            deviceAuthenticationStrategy.UnlinkDevice()
                .First()
                .Subscribe(isSuccessful =>
                {
                    PlayFabClientAPI.ForgetAllCredentials();
                    clientState.Reset();
                    clientState.onLogout.OnNext(Unit.Default);

                    clientState.isLocked.Value = false;
                });
        }

        public void LogoutFacebook()
        {
            Debug.Log("Logging out of fb");
            clientState.isLocked.Value = true;
            clientState.Reset();

            deviceAuthenticationStrategy.UnlinkDevice()
                .First()
                .Subscribe(isSuccessful =>
                {
                    clientState.isLocked.Value = false;
                    if (isSuccessful)
                    {
                        Debug.Log("Device unlinked!");
                        if (config.autoLogin)
                            LoginAsGuest ( () => { } );
                    }
                });
        }

        public void LoginAsGuest ( Action restorePurchase )
        {
            LoginWithDevice ( true, restorePurchase );
        }

        public IObservable<bool> LinkDevice()
        {
            return deviceAuthenticationStrategy.LinkDevice();
        }

        private void LoginWithDevice ( bool createAccount, Action restorePurchase )
        {
            Debug.Log("LoginWithDevice has account info cached: " + accountInfoController.HasAccountInfoCached);
            clientState.isLocked.Value = true;

            deviceAuthenticationStrategy.LoginWithDevice(createAccount)
                .First()
                .Subscribe(isSuccessful =>
                {
                    clientState.isLocked.Value = false;
                    if (isSuccessful)
                    {
                        Debug.Log("Frictionless login successful");
                        clientState.isLoggedIn.Value = true;
                        accountInfoController.RefreshAccountInfo();

                        restorePurchase?.Invoke ();
                    }
                });
        }

        private void AddContactEmail(string email)
        {
            PlayFabClientAPI.AddOrUpdateContactEmail(new AddOrUpdateContactEmailRequest
            {
                EmailAddress = email
            },
                _ => { },
                error => Debug.LogError(error.GenerateErrorReport()));
        }

        public void DeleteMyAccount ()
        {
            ExecuteCloudScriptRequest executeCloudScript = new ExecuteCloudScriptRequest
            {
                FunctionName = "DeleteMyData"
            };
            PlayFabClientAPI.ExecuteCloudScript ( executeCloudScript, ( ExecuteCloudScriptResult result ) =>
            {
                Debug.Log ( "account deleted" );
            },
            ( PlayFabError error ) =>
            {
                Debug.Log ( error.Error.ToString () );
            } );
        }
    }

    public class IOSDeviceAuthenticationStrategy : IDeviceAuthenticationStrategy
    {
        public string deviceId;

        public IOSDeviceAuthenticationStrategy(string deviceId)
        {
            this.deviceId = deviceId;
        }
        public IObservable<bool> LinkDevice()
        {
            Subject<bool> returned = new Subject<bool>();
            PlayFabClientAPI.LinkIOSDeviceID(new LinkIOSDeviceIDRequest
            {
                ForceLink = true,
                DeviceId = deviceId,
                DeviceModel = SystemInfo.deviceModel
            },
                result => returned.OnNext(true),
                error =>
                {
                    Debug.LogError(error.GenerateErrorReport());
                    returned.OnNext(false);
                });

            return returned;
        }

        public IObservable<bool> LoginWithDevice(bool createAccount)
        {
            Subject<bool> returned = new Subject<bool>();
            PlayFabClientAPI.LoginWithIOSDeviceID(new LoginWithIOSDeviceIDRequest
            {
                CreateAccount = createAccount,
                DeviceId = deviceId,
                DeviceModel = SystemInfo.deviceModel
            },
                result => returned.OnNext(true),
                error =>
                {
                    Debug.LogError(error.GenerateErrorReport());
                    returned.OnNext(false);
                });

            return returned;
        }

        public IObservable<bool> UnlinkDevice()
        {
            Subject<bool> returned = new Subject<bool>();
            PlayFabClientAPI.UnlinkIOSDeviceID(new UnlinkIOSDeviceIDRequest
            {
                DeviceId = deviceId
            },
                result => returned.OnNext(true),
                error =>
                {
                    Debug.LogError(error.GenerateErrorReport());
                    returned.OnNext(false);
                });

            return returned;
        }
    }

    public class AndroidDeviceAuthenticationStrategy : IDeviceAuthenticationStrategy
    {
        public string deviceId;

        public AndroidDeviceAuthenticationStrategy(string deviceId)
        {
            this.deviceId = deviceId;
        }

        public IObservable<bool> LinkDevice()
        {
            Subject<bool> returned = new Subject<bool>();
            PlayFabClientAPI.LinkAndroidDeviceID(new LinkAndroidDeviceIDRequest
            {
                ForceLink = true,
                AndroidDeviceId = deviceId,
                AndroidDevice = SystemInfo.deviceModel
            },
                result => returned.OnNext(true),
                error =>
                {
                    Debug.LogError(error.GenerateErrorReport());
                    returned.OnNext(false);
                });

            return returned;
        }

        public IObservable<bool> LoginWithDevice(bool createAccount)
        {
            Subject<bool> returned = new Subject<bool>();
            PlayFabClientAPI.LoginWithAndroidDeviceID(new LoginWithAndroidDeviceIDRequest
            {
                CreateAccount = createAccount,
                AndroidDeviceId = deviceId,
                AndroidDevice = SystemInfo.deviceModel
            },
                result => returned.OnNext(true),
                error =>
                {
                    Debug.LogError(error.GenerateErrorReport());
                    returned.OnNext(false);
                });
            return returned;
        }

        public IObservable<bool> UnlinkDevice()
        {
            Subject<bool> returned = new Subject<bool>();
            PlayFabClientAPI.UnlinkAndroidDeviceID(new UnlinkAndroidDeviceIDRequest
            {
                AndroidDeviceId = deviceId
            },
                result => returned.OnNext(true),
                error =>
                {
                    Debug.LogError(error.GenerateErrorReport());
                    returned.OnNext(false);
                });

            return returned;
        }
    }
}