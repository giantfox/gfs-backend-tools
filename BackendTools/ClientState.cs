﻿using System.Collections.Generic;
using PlayerIOClient;
using PlayFab.ClientModels;
using UniRx;

namespace GFS.BackendTools
{
    public class ClientState
    {
        public ReactiveProperty<bool> isConnected = new ReactiveProperty<bool>(false);
        public ReactiveProperty<bool> isLocked = new ReactiveProperty<bool>(false);
        public ReactiveProperty<bool> hasLocalUser = new ReactiveProperty<bool>(false);
        public ReactiveProperty<bool> isLoggedIn = new ReactiveProperty<bool>(false);
        public ReactiveProperty<bool> isEmailLinked = new ReactiveProperty<bool>(false);
        public ReactiveProperty<bool> isEmailVerified = new ReactiveProperty<bool>(false);
        public ReactiveProperty<bool> isFBLinked = new ReactiveProperty<bool>(false);
        public ReactiveProperty<bool> isCloudLoggedIn = new ReactiveProperty<bool>(false);
        public ReactiveProperty<bool> storeInitialized = new ReactiveProperty<bool>(false);
        public ReactiveProperty<string> playerId = new ReactiveProperty<string>(null);
        public ReactiveProperty<string> fbUserId = new ReactiveProperty<string>(null);
        public ReactiveProperty<string> selectedCharacterId = new ReactiveProperty<string>();
        public ReactiveProperty<string> errorMessage = new ReactiveProperty<string>(null);

        public Subject<Unit> onLogout = new Subject<Unit>();

        public void Reset()
        {
            hasLocalUser.Value = false;
            isLoggedIn.Value = false;
            isEmailLinked.Value = false;
            isEmailVerified.Value = false;
            isFBLinked.Value = false;
            isCloudLoggedIn.Value = false;
            storeInitialized.Value = false;
            playerId.Value = string.Empty;
            fbUserId.Value = string.Empty;
            selectedCharacterId.Value = string.Empty;
            errorMessage.Value = string.Empty;
        }
    }
}