﻿using System.Collections.Generic;
using System.Linq;
using PlayFab;
using PlayFab.ClientModels;
using UniRx;
using UnityEngine;
using Zenject;

namespace GFS.BackendTools
{
    public interface ICharacterRespositoryProvider<T>
    {
        IRepository<T> CurrentCharacterRespository { get; }
        IRepository<T> GetCharacterRespository ( string characterId );
        void DeleteKey ( string key );
        List<string> CharacterIds ();
    }

    public class CharacterRespositoryProvider<T> : ICharacterRespositoryProvider<T>, IInitializable
    {
        [Inject] ClientState clientState;
        [Inject] DiContainer container;

        private string prefix;

        private Dictionary<string, CharacterDataRepository<T>> otherRepositories = new Dictionary<string, CharacterDataRepository<T>> ();
        public IRepository<T> CurrentCharacterRespository { get; private set; }

        public CharacterRespositoryProvider ( string prefix )
        {
            this.prefix = prefix;
        }

        public void Initialize ()
        {
            var newRepo = container.Instantiate<CharacterDataRepository<T>> ( new List<object> { prefix, clientState.selectedCharacterId } );
            CurrentCharacterRespository = newRepo;
        }

        public IRepository<T> GetCharacterRespository ( string characterId )
        {
            if ( !otherRepositories.ContainsKey ( characterId ) )
            {
                //Debug.Log ( "it will be created: " + characterId );

                //return null;

                var newRepo = container.Instantiate<CharacterDataRepository<T>> ( new List<object> { prefix, new ReactiveProperty<string> ( characterId ) } );
                otherRepositories[ characterId ] = newRepo;
            }

            return otherRepositories[ characterId ];
        }

        public List<string> CharacterIds ()
        {
            List<string> charIDs = new List<string> ();

            foreach ( var key in otherRepositories )
            {
                charIDs.Add ( key.Key );
            }

            return charIDs;
        }

        public void DeleteKey ( string key )
        {
            Debug.Log ( otherRepositories.Count );

            otherRepositories.Remove ( key );

            Debug.Log ( otherRepositories.Count );
        }
    }

    public class CharacterDataRepository<T> : IRepository<T>
    {
        [Inject] public ICharacterDataController characterDataController;

        string prefix;
        IReadOnlyReactiveProperty<string> characterId;
        Dictionary<string, IReadOnlyReactiveProperty<T>> cache = new Dictionary<string, IReadOnlyReactiveProperty<T>>();

        ReactiveProperty<List<string>> usedKeys;

        public CharacterDataRepository(string prefix, IReadOnlyReactiveProperty<string> characterId)
        {
            this.prefix = prefix;
            this.characterId = characterId;
        }

        public IReadOnlyReactiveProperty<List<string>> Keys
        {
            get
            {
                if (usedKeys == null)
                {
                    usedKeys = new ReactiveProperty<List<string>>(new List<string>());

                    var disposables = new CompositeDisposable();

                    characterId
                        .Subscribe(newId =>
                        {
                            disposables.Clear();

                            characterDataController.Keys(newId)
                                .Select(newDataKeys => newDataKeys.Where(k => k.StartsWith(prefix, System.StringComparison.Ordinal)))
                                .Select(newDataKeys => newDataKeys.Select(k => k.Remove(0, prefix.Length)))
                                .Subscribe(newDataKeys => usedKeys.Value = newDataKeys.ToList())
                                .AddTo(disposables);
                        });
                }

                return usedKeys;
            }
        }

        public void DeleteKey(string key)
        {
            characterDataController.SetData(characterId.Value, new Dictionary<string, string> { { prefix + key, null } });
        }

        public IReadOnlyReactiveProperty<T> GetData(string key)
        {
            if ( !cache.ContainsKey ( key ) )
                //return null;
                cache[ key ] = characterDataController.GetData ( characterId.Value, prefix + key )
                    .GetConverted ( json => JsonUtility.FromJson<T> ( json ) );

            return cache[key];
        }

        List<Dictionary<string, string>> characterDataToBeSaved = default;

        public void SetData(string key, T newData, T deviceID )
        {
            Debug.Log ( deviceID );

            Dictionary<string, string> fixedData = new Dictionary<string, string> ()
            {
                { prefix + key, JsonUtility.ToJson(newData) },
                { prefix + "DeviceID", JsonUtility.ToJson( deviceID ) }
            };

            characterDataController.SetData ( characterId.Value, fixedData );
        }

        public void SetCharacterData ( string key, T newData )
        {
            if ( characterDataToBeSaved == null )
                characterDataToBeSaved = new List<Dictionary<string, string>> ();

            if ( characterDataToBeSaved.Count == 0 )
                characterDataToBeSaved.Add ( new Dictionary<string, string> () );

            Dictionary<string, string> currentCharacterDataPairs
                = characterDataToBeSaved[ characterDataToBeSaved.Count - 1 ];

            // now it's json, we don't need this anymore
            //if ( currentCharacterDataPairs.Count >= 5 )
            //{
            //    currentCharacterDataPairs = new Dictionary<string, string> ();
            //    characterDataToBeSaved.Add ( currentCharacterDataPairs );
            //}

            currentCharacterDataPairs.Add ( prefix + key, JsonUtility.ToJson ( newData ) );
        }

        public void SaveCharacterData ()
        {
            if ( characterDataToBeSaved == null )
                characterDataToBeSaved = new List<Dictionary<string, string>> ();

            IRepository<StringContainer> characterRepo = (IRepository<StringContainer>)this;

            // with this new code, characterDataToBeSaved have only one entry.
            // This needs to be refactored.
            foreach ( Dictionary<string, string>  data in characterDataToBeSaved )
            {
                Debug.Log ( Keys.Value.Count );

                foreach ( var k in Keys.Value )
                {
                    var displayedValue = GetData ( k ).Value;

                    if ( displayedValue is StringContainer )
                        data.Add ( k, (displayedValue as StringContainer).data );

                    Debug.Log ( "SS " + k );
                    Debug.Log ( "XX " + displayedValue );
                }

                characterDataController.SetData ( characterId.Value, data );
            }

            characterDataToBeSaved = new List<Dictionary<string, string>> ();
        }
    }
}