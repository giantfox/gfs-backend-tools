﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PlayFab;
using PlayFab.ClientModels;
using UniRx;
using UnityEngine;
using Zenject;

namespace GFS.BackendTools
{
    public class TitleDataRepository : IRepository<string>
    {
        [Inject] ClientState clientState;
        [Inject] ITitleDataController titleDataController;

        string prefix;

        private ReactiveProperty<List<string>> keys = new ReactiveProperty<List<string>>(new List<string>());
        public IReadOnlyReactiveProperty<List<string>> Keys => keys;

        public TitleDataRepository(string prefix)
        {
            this.prefix = prefix;
        }

        public IReadOnlyReactiveProperty<string> GetData(string key)
        {
            return titleDataController.GetData(prefix + key);
        }

        public void DeleteKey(string key)
        {
            titleDataController.SetData(key, null);
        }

        // Save title data somehow
        public void SetData(string key, string newData, string deviceID )
        {
            titleDataController.SetData(key, newData);
        }

        public void SetCharacterData ( string key, string newData )
        {

        }

        public void SaveCharacterData ()
        {

        }
    }
}