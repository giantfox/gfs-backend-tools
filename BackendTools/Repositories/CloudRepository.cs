﻿using System;
using System.Collections.Generic;
using GFS.Utilities;
using UniRx;
using UnityEngine;
using Zenject;

namespace GFS.BackendTools
{
    public interface ICloudSaveStrategy<D>
    {
        IObservable<D> LoadData(ICloudController cloudController, AssetAddress assetAddress);
        void SaveData(ICloudController cloudController, AssetAddress assetAddress, D data);
    }

    public class CloudRepository<T, D> : IRepository<T>, IInitializable, ICommunityRepositoryController<T> where T : IOwnedData
    {
        [Inject]
        private FriendsListController friendsListController;
        [Inject]
        private ICloudController cloudController;
        [Inject]
        private ClientState clientState;

        private Dictionary<string, ReactiveProperty<T>> data = new Dictionary<string, ReactiveProperty<T>>();
        private ReactiveProperty<List<string>> keys = new ReactiveProperty<List<string>>(new List<string>());

        private string tableId;
        private IDataSerializer<T, D> serializer;
        private ICloudSaveStrategy<D> saveStrategy;

        public IReadOnlyReactiveProperty<List<string>> Keys => keys;

        public IReadOnlyReactiveProperty<T> GetData(string key)
        {
            if (!data.ContainsKey(key))
                data[key] = new ReactiveProperty<T>();
            return data[key];
        }

        public CloudRepository(string tableId, IDataSerializer<T, D> serializer, ICloudSaveStrategy<D> saveStrategy)
        {
            this.tableId = tableId;
            this.serializer = serializer;
            this.saveStrategy = saveStrategy;
        }

        public void Initialize()
        {
            clientState.isCloudLoggedIn
                .DistinctUntilChanged()
                .Where(isCloudLoggedIn => isCloudLoggedIn)
                .Subscribe(_ =>
                {
                    LoadUserData(clientState.playerId.Value);
                });

            clientState.isCloudLoggedIn
                .DistinctUntilChanged()
                .Where(isCloudLoggedIn => !isCloudLoggedIn)
                .Subscribe(_ =>
                {
                    data.Clear();
                    keys.Value = new List<string>();
                });
        }

        public void SetData(string key, T newData, T deviceID )
        {
            bool keyAdded = false;
            if (!data.ContainsKey(key))
            {
                data.Add(key, new ReactiveProperty<T>());
                keyAdded = true;
            }
            data[key].Value = newData;

            if (keyAdded)
                keys.Value = new List<string>(data.Keys);

            saveStrategy.SaveData(cloudController, new AssetAddress { tableId = tableId, key = clientState.playerId.Value, property = key }, serializer.SerializeData(newData));

            //finished.
        }

        public void SetCharacterData ( string key, T newData )
        {

        }

        public void SaveCharacterData ()
        {

        }

        public void DeleteKey(string key)
        {
            if (data.Remove(key))
                keys.Value = new List<string>(data.Keys);

            cloudController.Delete(new AssetAddress { tableId = tableId, key = clientState.playerId.Value, property = key });
        }

        public void LoadUserData(string playerId)
        {
            clientState.isCloudLoggedIn
                .DistinctUntilChanged()
                .Where(isCloudLoggedIn => isCloudLoggedIn)
                .TakeUntil(clientState.isLoggedIn.Where(isLoggedIn => !isLoggedIn))
                .Subscribe(_ =>
                {
                    // Load from cloud
                    cloudController.LoadKeyProperties(new AssetAddress { tableId = tableId, key = playerId })
                        .Subscribe(properties =>
                        {
                            foreach (var a in properties)
                            {
                                var property = a;

                                var address = new AssetAddress { tableId = tableId, key = playerId, property = property };
                                Debug.Log("Loading: " + address);
                                saveStrategy.LoadData(cloudController, address)
                                    .Select(data => serializer.DeserializeData(data))
                                    .Subscribe(data =>
                                    {
                                        if (!this.data.ContainsKey(property))
                                            this.data[property] = new ReactiveProperty<T>();
                                        this.data[property].Value = data;
                                        var keyList = new List<string>(this.keys.Value);
                                        keyList.Add(property);
                                        this.keys.Value = keyList;
                                    });
                            }
                        });
                });
        }
    }

    public class SaveStringCloudStrategy : ICloudSaveStrategy<string>
    {
        public IObservable<string> LoadData(ICloudController cloudController, AssetAddress assetAddress)
        {
            return cloudController.LoadString(assetAddress);
        }

        public void SaveData(ICloudController cloudController, AssetAddress assetAddress, string data)
        {
            cloudController.SaveString(assetAddress, data);
        }
    }

    public class SaveBinaryCloudStrategy : ICloudSaveStrategy<byte[]>
    {
        public IObservable<byte[]> LoadData(ICloudController cloudController, AssetAddress assetAddress)
        {
            return cloudController.LoadAsset(assetAddress);
        }

        public void SaveData(ICloudController cloudController, AssetAddress assetAddress, byte[] data)
        {
            cloudController.SaveAsset(assetAddress, data);
        }
    }
}