﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GFS.BackendTools
{
    public interface ICloudController
    {
        void SaveString(AssetAddress assetAddress, string data);
        void SaveAsset(AssetAddress assetAddress, Byte[] data);
        IObservable<ICollection<string>> LoadKeyProperties(AssetAddress assetAddress);
        IObservable<byte[]> LoadAsset(AssetAddress assetAddress);
        IObservable<string> LoadString(AssetAddress assetAddress);
        void Delete(AssetAddress assetAddress);
    }

    public class StubCloudController : ICloudController
    {
        public void Delete(AssetAddress assetAddress)
        {
            throw new NotImplementedException();
        }

        public IObservable<byte[]> LoadAsset(AssetAddress assetAddress)
        {
            throw new NotImplementedException();
        }

        public IObservable<ICollection<string>> LoadKeyProperties(AssetAddress assetAddress)
        {
            throw new NotImplementedException();
        }

        public IObservable<string> LoadString(AssetAddress assetAddress)
        {
            throw new NotImplementedException();
        }

        public void SaveAsset(AssetAddress assetAddress, byte[] data)
        {
            throw new NotImplementedException();
        }

        public void SaveString(AssetAddress assetAddress, string data)
        {
            throw new NotImplementedException();
        }
    }
}