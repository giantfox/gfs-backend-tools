﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace GFS.BackendTools
{
    public interface ISocialController
    {
        void Login();
    }

    public class StubSocialController : ISocialController
    {
        public void Login()
        {
            throw new NotImplementedException();
        }
    }
}