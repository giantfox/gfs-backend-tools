﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using Zenject;

namespace GFS.BackendTools
{
    public class ConnectivityController : IInitializable
    {
        [Inject] ClientState clientState;
        public void Initialize()
        {
            Observable.EveryLateUpdate()
                .Subscribe(_ => clientState.isConnected.Value = Application.internetReachability != NetworkReachability.NotReachable);
        }
    }
}