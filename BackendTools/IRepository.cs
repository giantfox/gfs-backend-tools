﻿using System.Collections.Generic;
using System.Text;
using GFS.Utilities;
using UniRx;
using UnityEngine;

namespace GFS.BackendTools
{
    public interface IRepository<T>
    {
        IReadOnlyReactiveProperty<List<string>> Keys { get; }
        IReadOnlyReactiveProperty<T> GetData(string key);
        void SetData ( string key, T newData, T deviceID );
        void SetCharacterData ( string key, T newData );
        void SaveCharacterData ();
        void DeleteKey(string key);
    }

    public interface ICommunityRepositoryController<T> where T : IOwnedData
    {
        void LoadUserData(string userId);
    }

    public interface IDataSerializer<T, D>
    {
        D SerializeData(T data);
        T DeserializeData(D data);
    }

    public class JSONSerializer<T> : IDataSerializer<T, string>
    {
        public T DeserializeData(string data)
        {
            return JsonUtility.FromJson<T>(data);
        }

        public string SerializeData(T data)
        {
            return JsonUtility.ToJson(data);
        }
    }

    public class JSONBinarySerializer<T> : IDataSerializer<T, byte[]>
    {
        public T DeserializeData(byte[] data)
        {
            return JsonUtility.FromJson<T>(Encoding.ASCII.GetString(data));
        }

        public byte[] SerializeData(T data)
        {
            return Encoding.ASCII.GetBytes(JsonUtility.ToJson(data));
        }
    }
}