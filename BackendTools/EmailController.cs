﻿using System.Collections;
using System.Collections.Generic;
using PlayFab;
using PlayFab.ClientModels;
using UniRx;
using UnityEngine;
using Zenject;
using System.Linq;
using System.Text;
using SimpleJSON;

namespace GFS.BackendTools
{
    public class EmailController : IInitializable
    {
        private ReactiveProperty<bool> emailIsSubscribed = new ReactiveProperty<bool>();
        public IReadOnlyReactiveProperty<bool> EmailIsSubscribed => emailIsSubscribed;

        private string StatKey => StatisticsUtils.GenerateStatKey("o:subscribeToEmail");

        [Inject] ClientState clientState;

        public void Initialize()
        {
            clientState.isLoggedIn
                .Where(isLoggedIn => isLoggedIn)
                .Subscribe(isLoggedIn =>
                {
                    PlayFabClientAPI.GetPlayerStatistics(new GetPlayerStatisticsRequest
                    {
                        StatisticNames = new List<string> { StatKey }
                    }, results =>
                    {
                        var stat = results.Statistics.FirstOrDefault(s => s.StatisticName == StatKey);
                        if (stat != null)
                            emailIsSubscribed.Value = stat.Value == 1;
                        else
                        {
                            emailIsSubscribed.Value = true;
                            ToggleEmailSubscription(true);
                        }
                    }, error => Debug.LogError(error.GenerateErrorReport()));
                });
        }

        public void SendRecoveryEmail(string email)
        {
            PlayFabClientAPI.SendAccountRecoveryEmail(new SendAccountRecoveryEmailRequest
            {
                Email = email,
                EmailTemplateId = "BC1463FAD46943B3",
                TitleId = PlayFabSettings.TitleId

            }, results =>
            {
                Debug.Log("Email sent!");
            },
            error =>
            {
                Debug.LogError(error.GenerateErrorReport());
                clientState.errorMessage.Value = error.GenerateUserFriendlyReport();
            });
        }

        public void ResendRegistrationVerificationEmail ()
        {
            ExecuteCloudScriptRequest executeCloudScript = new ExecuteCloudScriptRequest
            {
                FunctionName = "ResendRegistrationVerificationEmail"
            };
            PlayFabClientAPI.ExecuteCloudScript ( executeCloudScript,
            ( ExecuteCloudScriptResult result ) =>
            {
                Debug.Log ( "ResendRegistrationVerificationEmail called" );
            },
            ( PlayFabError error ) =>
            {
                Debug.Log ( error.Error.ToString () );
            } );
        }

        public void ToggleEmailSubscription(bool isSubscribed)
        {
            emailIsSubscribed.Value = isSubscribed;
            PlayFabClientAPI.UpdatePlayerStatistics(new UpdatePlayerStatisticsRequest
            {
                Statistics = new List<StatisticUpdate>
                {
                    new StatisticUpdate
                    {
                        StatisticName = StatKey,
                        Value = isSubscribed?1:0
                    }
                }
            }, results => { }
            , error => Debug.LogError(error.GenerateErrorReport()));
        }

        public void SaveContactEmailsToClipboard()
        {
#if UNITY_EDITOR
            PlayFabClientAPI.ExecuteCloudScript(new ExecuteCloudScriptRequest
            {
                FunctionName = "getSubscribedEmails",
                GeneratePlayStreamEvent = true,
                FunctionParameter = new
                {
                    pw = "3Pj+GExYpTv_mK_*"
                }
            }, results =>
            {
                var sb = new StringBuilder();
                var emailList = JSON.Parse(results.FunctionResult.ToString());
                foreach (var e in emailList)
                {
                    sb.Append(e.Value.ToString().Replace("\"", ""));
                    sb.Append("\n");
                }
                GUIUtility.systemCopyBuffer = sb.ToString();
                Debug.Log(sb.ToString());
            }, error => Debug.LogError(error.GenerateErrorReport()));
#endif
        }
    }
}