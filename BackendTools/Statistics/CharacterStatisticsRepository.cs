﻿using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using Zenject;

namespace GFS.BackendTools
{
    public class CharacterStatisticsRepository : IStatisticsRepository
    {
        [Inject] ClientState clientState;
        [Inject] ICharacterDataController characterDataBackendCache;

        private IReadOnlyReactiveProperty<string> characterId;
        private Dictionary<string, IReadOnlyReactiveProperty<StatisticHistory>> cache = new Dictionary<string, IReadOnlyReactiveProperty<StatisticHistory>>();

        public CharacterStatisticsRepository(IReadOnlyReactiveProperty<string> characterId)
        {
            this.characterId = characterId;
        }

        void IStatisticsRepository.AddNewStatistic(string statId, int value)
        {
            var returned = new Subject<Unit>();

            var statKey = StatisticsUtils.GenerateStatKey(statId);

            var newStatisticHistory = GetHistory(statId);
            newStatisticHistory.Value.statistics = new List<HistoryStatistic>(newStatisticHistory.Value.statistics)
            {
                new HistoryStatistic
                {
                    value = value,
                    dateTime = DateTime.Now.ToUniversalTime().ToString()
                }
            };

            characterDataBackendCache.SetData(characterId.Value, new Dictionary<string, string>
                {
                    {statKey, JsonUtility.ToJson(newStatisticHistory.Value)}
                });
        }

        public IReadOnlyReactiveProperty<StatisticHistory> GetHistory(string statId)
        {
            var statKey = StatisticsUtils.GenerateStatKey(statId);
            if (!cache.ContainsKey(statId))
            {
                var newStat = new ReactiveProperty<StatisticHistory>();

                var disposables = new CompositeDisposable();
                characterId
                    .Subscribe(currentCharacterId =>
                    {
                        disposables.Clear();

                        characterDataBackendCache.GetData(characterId.Value, statKey)
                            .Subscribe(dataJson =>
                            {
                                newStat.Value = !string.IsNullOrEmpty(dataJson) ? JsonUtility.FromJson<StatisticHistory>(dataJson) : new StatisticHistory
                                {
                                    metadata = new StatisticMetadata
                                    {
                                        statId = statId,
                                        playerId = clientState.playerId.Value,
                                        characterId = currentCharacterId
                                    },
                                    statistics = new List<HistoryStatistic>()
                                };
                            })
                            .AddTo(disposables);
                    });

                cache[statId] = newStat;
            }

            return cache[statId];
        }
    }
}