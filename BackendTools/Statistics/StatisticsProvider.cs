﻿using System.Collections.Generic;
using UniRx;
using Zenject;

namespace GFS.BackendTools
{
    public interface IStatisticsProvider
    {
        IStatisticsRepository PlayerStatistics { get; }
        IStatisticsRepository SelectedCharacterStatistics { get; }
        IStatisticsRepository GetCharacterStatistics(string characterId);
    }

    public class StatisticsProvider : IStatisticsProvider
    {
        private IStatisticsRepository playerStatisticsController;
        private IStatisticsRepository selectedCharacterStatisticsController;

        [Inject] DiContainer container;
        [Inject] ClientState clientState;

        Dictionary<string, IStatisticsRepository> characterControllers = new Dictionary<string, IStatisticsRepository>();

        public IStatisticsRepository PlayerStatistics
        {
            get
            {
                if (playerStatisticsController == null)
                    playerStatisticsController = container.Instantiate<UserStatisticsRepository>();

                return playerStatisticsController;
            }
        }

        public IStatisticsRepository SelectedCharacterStatistics
        {
            get
            {
                if (selectedCharacterStatisticsController == null)
                    selectedCharacterStatisticsController = container.Instantiate<CharacterStatisticsRepository>(new List<object> { clientState.selectedCharacterId });

                return selectedCharacterStatisticsController;
            }
        }


        public IStatisticsRepository GetCharacterStatistics(string characterId)
        {
            if (!characterControllers.ContainsKey(characterId))
            {
                var newStatisticsRepository = container.Instantiate<CharacterStatisticsRepository>(new List<object> { new ReactiveProperty<string>(characterId) });
                characterControllers[characterId] = newStatisticsRepository;
            }

            return characterControllers[characterId];
        }
    }
}