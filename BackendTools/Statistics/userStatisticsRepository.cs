﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PlayFab;
using PlayFab.ClientModels;
using UniRx;
using UnityEngine;
using Zenject;

namespace GFS.BackendTools
{
    public class UserStatisticsRepository : IStatisticsRepository
    {
        [Inject] ClientState clientState;
        [Inject] IUserDataController userDataBackendCache;

        public void AddNewStatistic(string statId, int value)
        {
            var returned = new Subject<Unit>();

            var statKey = StatisticsUtils.GenerateStatKey(statId);

            var newStatisticHistory = GetHistory(statId);
            newStatisticHistory.Value.statistics = new List<HistoryStatistic>(newStatisticHistory.Value.statistics)
            {
                new HistoryStatistic
                {
                    value = value,
                    dateTime = DateTime.Now.ToUniversalTime().ToString()
                }
            };

            userDataBackendCache
                .SetData(new Dictionary<string, string>
                {
                    {statKey,JsonUtility.ToJson(newStatisticHistory)}
                });
        }

        public IReadOnlyReactiveProperty<StatisticHistory> GetHistory(string statId)
        {
            var returned = new ReactiveProperty<StatisticHistory>();
            var statKey = StatisticsUtils.GenerateStatKey(statId);

            userDataBackendCache.GetData(statKey)
                .Subscribe(dataJson =>
                {
                    returned.Value = !string.IsNullOrEmpty(dataJson) ? JsonUtility.FromJson<StatisticHistory>(dataJson) : new StatisticHistory
                    {
                        metadata = new StatisticMetadata
                        {
                            statId = statId,
                            playerId = clientState.playerId.Value
                        },
                        statistics = new List<HistoryStatistic>()
                    };
                });

            return returned;
        }
    }
}