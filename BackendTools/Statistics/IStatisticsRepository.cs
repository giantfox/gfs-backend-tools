﻿using System;
using System.Collections.Generic;
using UniRx;

namespace GFS.BackendTools
{
    public interface IStatisticsRepository
    {
        void AddNewStatistic(string statId, int value);
        IReadOnlyReactiveProperty<StatisticHistory> GetHistory(string statId);
    }

    [Serializable]
    public class StatisticHistory
    {
        public StatisticMetadata metadata;
        public List<HistoryStatistic> statistics;
    }

    [Serializable]
    public class StatisticMetadata
    {
        public string statId;
        public string playerId;
        public string characterId;
    }

    [Serializable]
    public class HistoryStatistic
    {
        public int value;
        public string dateTime;
    }

    public static class StatisticsUtils
    {
        private const string statPrefix = "s:";

        public static bool IsStatKey(string key)
        {
            return key.StartsWith(statPrefix, StringComparison.Ordinal);
        }

        public static string GenerateStatKey(string statId)
        {
            return statPrefix + statId;
        }
    }
}