﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using PlayFab;
using PlayFab.Json;
using UniRx;
using UnityEngine;
using static PlayFab.Json.SimpleJsonInstance;

namespace GFS.BackendTools
{
    public static class BackendUtils
    {
        public static string GenerateUserFriendlyReport(this PlayFabError error)
        {
            return error.ErrorDetails?.FirstOrDefault().Value?.FirstOrDefault() ?? error.ErrorMessage;
        }

        public static string RemoveAllNonAlphaNumeric(this string str)
        {
            Regex rgx = new Regex("[^a-zA-Z0-9 -]");
            return rgx.Replace(str, "");
        }

        public static string StringArrayToString(this string[] stringArray)
        {
            var sb = new StringBuilder();

            for (int a = 0; a < stringArray.Length; a++)
            {
                sb.Append(stringArray[a]);
                if (a < stringArray.Length - 1)
                    sb.Append("/");
            }

            return sb.ToString();
        }

        public static string[] StringToStringArray(this string stringData)
        {
            return stringData.Split('/');
        }

        public static IReadOnlyReactiveProperty<T> GetConverted<X, T>(this IReadOnlyReactiveProperty<X> property, Func<X, T> convertTo)
        {
            return new ReadonlyConvertedReactiveProperty<X, T>(property, convertTo);
        }

        public static IReactiveProperty<T> GetConverted<X, T>(this IReactiveProperty<X> property, Func<X, T> convertTo, Func<T, X> convertFrom)
        {
            return new ConvertedReactiveProperty<X, T>(property, convertTo, convertFrom);
        }

        static IJsonSerializerStrategy deserializationStrategy = new PlayFabSimpleJsonCuztomization();
        public static T DeserializePlayfabClass<T>(string json)
        {
            if (string.IsNullOrEmpty(json))
                return default;
            return (T)PlayFabSimpleJson.DeserializeObject(json, typeof(T), deserializationStrategy);
        }
    }


    public class ReadonlyConvertedReactiveProperty<X, T> : IReadOnlyReactiveProperty<T>
    {
        protected Func<X, T> convertTo;
        protected IReadOnlyReactiveProperty<X> readonlyProperty;

        public ReadonlyConvertedReactiveProperty(IReadOnlyReactiveProperty<X> property, Func<X, T> convertTo)
        {
            this.readonlyProperty = property;
            this.convertTo = convertTo;
        }

        public T Value { get => convertTo(readonlyProperty.Value); }

        public bool HasValue => readonlyProperty.HasValue;

        public IDisposable Subscribe(IObserver<T> observer)
        {
            return readonlyProperty
                .Select(v => convertTo(v))
                .Subscribe(observer);
        }
    }

    public class ConvertedReactiveProperty<X, T> : ReadonlyConvertedReactiveProperty<X, T>, IReactiveProperty<T>
    {
        protected IReactiveProperty<X> property;
        protected Func<T, X> convertFrom;

        public ConvertedReactiveProperty(IReactiveProperty<X> property, Func<X, T> convertTo, Func<T, X> convertFrom) : base(property, convertTo)
        {
            this.property = property;
            this.convertFrom = convertFrom;
        }

        T IReactiveProperty<T>.Value { get => convertTo(property.Value); set => property.Value = convertFrom(value); }
    }
}