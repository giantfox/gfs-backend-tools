﻿using UnityEngine;
using Zenject;

namespace GFS.BackendTools
{
    public class BackendToolsInstaller : MonoInstaller
    {
        [System.Serializable]
        public class Config
        {
            public bool useCloud = true;
            public bool useFB = true;
        }

        [SerializeField] Config config;
        [SerializeField] PlayerIOController.Config playerIOConfig;
        [SerializeField] AuthenticationController.Config authenticationConfig;

        public override void InstallBindings()
        {
            Container.BindInterfacesTo<AlertController>()
                .AsSingle()
                .NonLazy();
            Container.BindInterfacesAndSelfTo<ClientState>()
                .AsSingle()
                .NonLazy();
            Container.BindInterfacesAndSelfTo<AuthenticationController> ()
                .AsSingle ()
                .WithArguments ( authenticationConfig )
                .NonLazy ();
            Container.BindInterfacesTo<ConnectivityController>()
                .AsSingle()
                .NonLazy();
            Container.BindInterfacesAndSelfTo<FriendsListController>()
                .AsSingle()
                .NonLazy();
            Container.BindInterfacesAndSelfTo<EmailController>()
                .AsSingle()
                .NonLazy();
            Container.BindInterfacesTo<StatisticsProvider>()
                .AsSingle()
                .NonLazy();
            Container.BindInterfacesTo<PurchasesController>()
                .AsSingle()
                .NonLazy();

            if (config.useCloud)
                //CloudController
                Container.BindInterfacesTo<PlayerIOController>()
                    .AsSingle()
                    .WithArguments(playerIOConfig)
                    .NonLazy();
            else
                Container.BindInterfacesTo<StubCloudController>()
                    .AsSingle()
                    .NonLazy();

            /*
            if (config.useFB)
                //CloudController
                Container.BindInterfacesTo<FBController>()
                    .AsSingle()
                    .NonLazy();
            else
            */
                Container.BindInterfacesTo<StubSocialController>()
                    .AsSingle()
                    .NonLazy();

            Container.BindInterfacesTo<UserDataBackendCache>()
                .AsSingle()
                .NonLazy();
            Container.BindInterfacesTo<CharacterDataBackendCache>()
                .AsSingle()
                .NonLazy();
            Container.BindInterfacesTo<CharacterProfileBackendCache>()
                .AsSingle()
                .NonLazy();
            Container.BindInterfacesTo<TitleDataBackendCache>()
                .AsSingle()
                .NonLazy();
            Container.BindInterfacesTo<AccountInfoBackendCache>()
                .AsSingle()
                .NonLazy();
        }
    }
}