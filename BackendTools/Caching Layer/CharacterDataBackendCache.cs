﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using System;
using UniRx;
using PlayFab.ClientModels;
using Zenject;
using System.Linq;
using Newtonsoft.Json;
using SimpleJSON;

public class AsyncProcessor : MonoBehaviour
{
    // Purposely left empty
}

namespace GFS.BackendTools
{
    public interface ICharacterDataController
    {
        IReadOnlyReactiveProperty<string> GetData(string characterId, string key);
        void SetData(string characterId, Dictionary<string, string> data);
        IReadOnlyReactiveProperty<string[]> Keys(string characterId);
        void MigrateOldDataAndStopSavingLocalData ( ICharacterRespositoryProvider<StringContainer> characterRepoProvider );
        Dictionary<string, string> GetSavedData ();
    }

    public class CharacterDataBackendCache : BackendCache, ICharacterDataController
    {
        private class CharacterDataKey
        {
            public string characterId;
            public string key;
        }

        [Inject] ICharacterProfileController characterProfileBackendCache;
        [Inject] AuthenticationController authenticationController;

        ICharacterRespositoryProvider<StringContainer> characterRepoProvider;

        protected override string DiskKey => "CharacterData";

        private Dictionary<string, IReadOnlyReactiveProperty<string[]>> keyCache = new Dictionary<string, IReadOnlyReactiveProperty<string[]>>();

        public IReadOnlyReactiveProperty<string> GetData(string characterId, string key)
        {
            var usedKey = GenerateKey(characterId, key);
            return GetData(usedKey);
        }

        public Dictionary<string, string> GetSavedData ()
        {
            return cache
                        .Where ( kvp => backlog.Contains ( kvp.Key ) )
                        .ToDictionary ( kvp => kvp.Key, kvp => kvp.Value.Value );
        }

        public void SetData(string characterId, Dictionary<string, string> data)
        {
            SetData(data
                .ToDictionary(kvp => GenerateKey(characterId, kvp.Key), kvp2 => kvp2.Value));
        }

        public IReadOnlyReactiveProperty<string[]> Keys(string characterId)
        {
            if (!keyCache.ContainsKey(characterId))
                keyCache[characterId] = keys.GetConverted(newKeys => newKeys
                    .Select(ParseKey)
                    .Where(d => d.characterId == characterId)
                    .Select(d => d.key)
                    .ToArray());

            return keyCache[characterId];
        }

        protected override IObservable<HashSet<string>> SendData(Dictionary<string, string> backlog)
        {
            var returned = new Subject<HashSet<string>>();
            var failedKeys = new HashSet<string>();

            var dataToSend = new Dictionary<string, Dictionary<string, string>>();

            foreach (var kvp in backlog)
            {
                var key = ParseKey(kvp.Key);

                if (!dataToSend.ContainsKey(key.characterId))
                    dataToSend[key.characterId] = new Dictionary<string, string>();

                dataToSend[key.characterId][key.key] = kvp.Value;
            }

            void SendCharacterData()
            {
                Debug.Log ( "SendCharacterData" );

                foreach ( string key in dataToSend.Keys )
                {
                    Debug.Log ( key );
                }

                if (dataToSend.Any())
                {
                    var characterData = dataToSend.First();

                    void HandleSendFailed()
                    {
                        foreach (var kvp in characterData.Value)
                        {
                            var failedKey = GenerateKey(characterData.Key, kvp.Key);
                            failedKeys.Add(failedKey);
                        }

                        dataToSend.Remove(characterData.Key);
                        SendCharacterData();
                    }

                    characterProfileBackendCache.LocalIdToRemoteId ( characterData.Key )
                        .First()
                        .Subscribe(remoteCharacterId =>
                        {
                            Dictionary<string, string> data = characterData.Value
                                    .Where ( kvp => !string.IsNullOrEmpty ( kvp.Value ) )
                                    .ToDictionary ( kvp => kvp.Key, kvp => kvp.Value );

                            string jsonData = JsonConvert.SerializeObject ( data );

                            Debug.Log ( "saving characters data: " + jsonData );

                            Dictionary<string, string> playfabData = new Dictionary<string, string> ();
                            playfabData.Add ( "data", jsonData );

                            //if ( characterProfileBackendCache.CharactersData ().Keys.Contains ( characterData.Key ) )
                            //{
                                PlayFabClientAPI.UpdateCharacterData ( new UpdateCharacterDataRequest
                                {
                                    CharacterId = remoteCharacterId,
                                    Data = playfabData
                                    //KeysToRemove = characterData.Value
                                    //.Where ( kvp => string.IsNullOrEmpty ( kvp.Value ) )
                                    //.Select ( kvp => kvp.Key )
                                    //.ToList ()
                                }, results =>
                                {
                                    dataToSend.Remove ( characterData.Key );
                                    SendCharacterData ();
                                },
                            error =>
                            {
                                Debug.LogError ( error.GenerateErrorReport () );
                                HandleSendFailed ();
                            } );
                            //}
                            //else
                            //{
                            //    var newCharacterId = characterProfileBackendCache.CreateCharacter ();

                            //    characterRepoProvider.GetCharacterRespository ( newCharacterId ).SetData (
                            //        "Name",
                            //        new StringContainer { data = characterData.Key },
                            //        new StringContainer { data = authenticationController.deviceId } );

                            //    dataToSend.Remove ( characterData.Key );
                            //    SendCharacterData ();
                            //}
                        });
                }
                else
                    returned.OnNext(failedKeys);
            }

            SendCharacterData();

            return returned;
        }

        public void MigrateOldDataAndStopSavingLocalData ( ICharacterRespositoryProvider<StringContainer> _characterRepoProvider )
        {
            characterRepoProvider = _characterRepoProvider;

            GameObject asyncGO = new GameObject ( "async" );
            AsyncProcessor _asyncProcessor = asyncGO.AddComponent<AsyncProcessor> ();

            _asyncProcessor.StartCoroutine ( MigrateData () );

            //RefreshCache ();



            //if ( PlayerPrefs.GetInt ( "migratedData", 0 ) == 0 )
            //{

            //PlayerPrefs.SetInt ( "migratedData", 1 );
            //}
            //else
            //{

            //}
        }

        IEnumerator MigrateData ()
        {
            Dictionary<string, string> dic = cache
                //.Where ( kvp => backlog.Contains ( kvp.Key ) )
                .ToDictionary ( kvp => kvp.Key, kvp => kvp.Value.Value );

            Debug.Log ( "----1---- " + characterProfileBackendCache.CharacterIds.Value.Length );
            Debug.Log ( "----1---- " + cache.Keys.Count );

            Dictionary<string, Dictionary<string, string>> oldCache = new Dictionary<string, Dictionary<string, string>> ();

            foreach ( string v in cache.Keys )
            {
                //Debug.Log ( v + " " + cache[v] );

                JSONNode keyNode = SimpleJSON.JSON.Parse ( v );
                JSONNode valueNode = SimpleJSON.JSON.Parse ( cache[ v ].Value );

                string characterId = SimpleJSON.JSON.Parse ( v )[ "characterId" ];
                string key = SimpleJSON.JSON.Parse ( v )[ "key" ];
                Debug.Log ( characterId );
                Debug.Log ( key );

                if ( valueNode != null && valueNode[ "data" ] != null )
                {
                    if ( !oldCache.Keys.Contains ( characterId ) )
                    {
                        oldCache.Add ( characterId, new Dictionary<string, string> () );
                    }

                    Dictionary<string, string> keys = oldCache[ characterId ];

                    keys.Add ( key, valueNode[ "data" ] );
                }
            }

            //Debug.Log ( "sss: " + characterRepoProvider.CharacterIds ().Count );

            foreach ( string key in oldCache.Keys )
            {
                var newCharacterId = characterProfileBackendCache.CreateCharacter ();
                var currentCharacterRepo = characterRepoProvider.GetCharacterRespository ( newCharacterId );

                Dictionary<string, string> characterKeys = oldCache[ key ];

                foreach ( string valueKey in characterKeys.Keys )
                {
                    currentCharacterRepo.SetCharacterData ( valueKey, new StringContainer { data = characterKeys[ valueKey ] } );
                }

                currentCharacterRepo.SaveCharacterData ();

                yield return new WaitForSecondsRealtime ( 1.5f );
            }

            //foreach ( var c in characterRepoProvider.CharacterIds () )
            //{
            //    //if ( characterProfileBackendCache.CharactersData () != null )
            //    //{
            //    //Debug.Log ( c + " " + characterProfileBackendCache.CharactersData ()[ c ] );
            //    var characterDisplayName = characterRepoProvider.GetCharacterRespository ( c ).GetData ( "Name" ).Value?.data;

            //    Debug.Log ( characterDisplayName );

            //    var newCharacterId = characterProfileBackendCache.CreateCharacter ();

            //    var currentCharacterRepo = characterRepoProvider.GetCharacterRespository ( newCharacterId );

            //    Debug.Log ( "FFFFFFFFFFFFFFFFFFF " + currentCharacterRepo.Keys.Value.Count );

            //    foreach ( var k in currentCharacterRepo.Keys.Value )
            //    {
            //        var displayedValue = currentCharacterRepo.GetData ( k ).Value?.data;

            //        Debug.Log ( displayedValue );

            //        currentCharacterRepo.SetCharacterData ( k, new StringContainer { data = displayedValue } );

            //        //currentCharacterRepo.SetData (
            //        //    "Name",
            //        //    new StringContainer { data = characterDisplayName },
            //        //    new StringContainer { data = authenticationController.deviceId } );
            //    }

            //    currentCharacterRepo.SaveCharacterData ();

            //    yield return new WaitForSecondsRealtime ( 1.5f );
            //}

            Debug.Log ( "--------" );

            yield return new WaitForSecondsRealtime ( 1.5f );

            RefreshData ();

            yield return new WaitForSecondsRealtime ( 3 );

            foreach ( var c in characterProfileBackendCache.CharacterIds.Value )
            {
                var character = characterRepoProvider.GetCharacterRespository ( c );

                character.SaveCharacterData ();
            }

            //SendData ( cache
            //        .Where ( kvp => backlog.Contains ( kvp.Key ) )
            //        .ToDictionary ( kvp => kvp.Key, kvp => kvp.Value.Value ) )
            //.First ()
            //.Subscribe ( newBacklog =>
            //{
            //    backlog = newBacklog;
            //    //cache.Clear ();
            //} );
        }

        public void RefreshData ()
        {
            RefreshCache ();
        }

        protected override IObservable<Dictionary<string, string>> RefreshCache()
        {
            var returned = new Subject<Dictionary<string, string>>();
            var data = new Dictionary<string, string>();

            PlayFabClientAPI.GetAllUsersCharacters(new ListUsersCharactersRequest(),
                result =>
                {
                    var characters = result.Characters;

                    void SendCharacterDataRequest()
                    {
                        if (characters.Any())
                        {
                            var characterData = characters.First();

                            PlayFabClientAPI.GetCharacterData(new GetCharacterDataRequest
                            {
                                CharacterId = characterData.CharacterId
                            }, characterResult =>
                            {
                                var hangingRequests = new List<string>();
                                foreach (var c in characterResult.Data)
                                {
                                    var requestId = Guid.NewGuid().ToString();
                                    hangingRequests.Add(requestId);
                                    characterProfileBackendCache.RemoteIdTLocalId(characterResult.CharacterId)
                                        .First()
                                        .Subscribe(localId =>
                                        {
                                            Dictionary<string, string> convertedObject =
                                                JsonConvert.DeserializeObject<Dictionary<string, string>> ( c.Value.Value );

                                            Debug.Log ( "getting characters data" );

                                            Debug.Log ( convertedObject );

                                            foreach ( string convertedKey in convertedObject.Keys )
                                            {
                                                var key = GenerateKey ( localId, convertedKey );
                                                data[ key ] = convertedObject[ convertedKey ];

                                                Debug.Log ( key + " " + data[ key ] );
                                            }

                                            Debug.Log ( "finished getting characters data" );

                                            hangingRequests.Remove(requestId);
                                            if (!hangingRequests.Any())
                                            {
                                                characters.Remove(characterData);
                                                SendCharacterDataRequest();
                                            }
                                        });
                                }
                            }, error =>
                            {
                                Debug.LogError(error.GenerateErrorReport());
                                characters.Remove(characterData);
                                SendCharacterDataRequest();
                            });
                        }
                        else
                            returned.OnNext(data);
                    }

                    SendCharacterDataRequest();
                }, error => Debug.LogError(error));

            return returned;
        }

        private string GenerateKey(string characterId, string key)
        {
            return JsonUtility.ToJson(new CharacterDataKey
            {
                characterId = characterId,
                key = key
            });
        }

        private CharacterDataKey ParseKey(string keyString)
        {
            return JsonUtility.FromJson<CharacterDataKey>(keyString);
        }
    }
}