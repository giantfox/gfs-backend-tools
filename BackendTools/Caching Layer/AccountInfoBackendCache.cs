﻿using System;
using System.Collections.Generic;
using PlayFab;
using PlayFab.ClientModels;
using UniRx;
using UnityEngine;

namespace GFS.BackendTools
{
    public interface IAccountInfoController
    {
        bool HasAccountInfoCached { get; }
        IReadOnlyReactiveProperty<GetPlayerCombinedInfoResultPayload> AccountInfo { get; }
        void RefreshAccountInfo();
    }

    public class AccountInfoBackendCache : ReadOnlyBackendCache, IAccountInfoController
    {
        private ReactiveProperty<GetPlayerCombinedInfoResultPayload> accountInfo = new ReactiveProperty<GetPlayerCombinedInfoResultPayload>();

        public IReadOnlyReactiveProperty<GetPlayerCombinedInfoResultPayload> AccountInfo => accountInfo;

        protected override string DiskKey => "AccountInfo";

        public bool HasAccountInfoCached
        {
            get
            {
                if (!cache.ContainsKey("_") || string.IsNullOrEmpty(cache["_"].Value))
                    return false;

                return !string.IsNullOrEmpty(BackendUtils.DeserializePlayfabClass<GetPlayerCombinedInfoResultPayload>(cache["_"].Value)?.AccountInfo?.PlayFabId);
            }
        }

        public override void Initialize()
        {
            base.Initialize();

            GetData("_")
                .GetConverted(json => BackendUtils.DeserializePlayfabClass<GetPlayerCombinedInfoResultPayload>(json) ?? default)
                .Subscribe(data => accountInfo.Value = data);
        }

        public void RefreshAccountInfo()
        {
            LaunchRefresh();
        }

        protected override IObservable<Dictionary<string, string>> RefreshCache()
        {
            Subject<Dictionary<string, string>> returned = new Subject<Dictionary<string, string>>();

            PlayFabClientAPI.GetPlayerCombinedInfo(new GetPlayerCombinedInfoRequest
            {
                InfoRequestParameters = new GetPlayerCombinedInfoRequestParams
                {
                    GetUserAccountInfo = true,
                    GetPlayerProfile = true,
                    ProfileConstraints = new PlayerProfileViewConstraints
                    {
                        ShowContactEmailAddresses = true,

                    }
                }
            },
                results =>
                {
                    returned.OnNext(new Dictionary<string, string> { { "_", results.InfoResultPayload.ToJson() } });
                },
                error =>
                {
                    returned.OnNext(new Dictionary<string, string>());
                    Debug.LogError(error.GenerateErrorReport());
                });

            return returned;
        }
    }
}