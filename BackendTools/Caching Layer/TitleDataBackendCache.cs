﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PlayFab;
using PlayFab.ClientModels;
using UniRx;
using UnityEngine;

namespace GFS.BackendTools
{
    public interface ITitleDataController
    {
        IReadOnlyReactiveProperty<string[]> Keys { get; }
        IReadOnlyReactiveProperty<string> GetData(string key);
        void SetData(string key, string data);
    }

    public class TitleDataBackendCache : BackendCache, ITitleDataController
    {
        public IReadOnlyReactiveProperty<string[]> Keys => keys;

        protected override string DiskKey => "TitleData";

        IReadOnlyReactiveProperty<string> ITitleDataController.GetData(string key)
        {
            return GetData(key);
        }

        public void SetData(string key, string data)
        {
            SetData(new Dictionary<string, string> { { key, data } });
        }

        protected override IObservable<Dictionary<string, string>> RefreshCache()
        {
            var returned = new Subject<Dictionary<string, string>>();

            PlayFabClientAPI.GetTitleData(new GetTitleDataRequest(), results =>
            {
                Debug.Log("Title data downloaded");
                returned.OnNext(results.Data);
            },
            error =>
            {
                Debug.LogError(error.GenerateErrorReport());
                returned.OnNext(null);
            });

            return returned;
        }

        protected override IObservable<HashSet<string>> SendData(Dictionary<string, string> backlog)
        {
            var returned = new Subject<HashSet<string>>();
            var failedKeys = new HashSet<string>();
#if UNITY_EDITOR
            PlayFabClientAPI.GetTitleData(new GetTitleDataRequest(),
                result =>
                {
                    var keysToSet = backlog
                        .Where(kvp => !string.IsNullOrEmpty(kvp.Value))
                        .ToList();

                    var keysToDelete = backlog
                        .Where(kvp => string.IsNullOrEmpty(kvp.Value))
                        .ToList();

                    void SendData()
                    {
                        if (keysToSet.Any())
                        {
                            var keyToSet = keysToSet.First();

                            void HandleFailed()
                            {
                                keysToSet.Remove(keyToSet);
                                failedKeys.Add(keyToSet.Key);

                                SendData();
                            }

                            PlayFabClientAPI.ExecuteCloudScript(new ExecuteCloudScriptRequest
                            {
                                FunctionName = "setTitleData",
                                FunctionParameter = new
                                {
                                    key = keyToSet.Key,
                                    value = JsonUtility.ToJson(keyToSet.Value),
                                    pw = "3Pj+GExYpTv_mK_*"
                                },
                                GeneratePlayStreamEvent = true
                            },
                                results =>
                                {
                                    if (results.Error == null)
                                    {
                                        Debug.Log(keyToSet.Key + " saved to Title Data!");

                                        keysToSet.Remove(keyToSet);
                                        SendData();
                                    }
                                    else
                                    {
                                        Debug.LogError(results.Error.Error + "\n" + results.Error.StackTrace);
                                        HandleFailed();
                                    }
                                },
                                error =>
                                {
                                    Debug.LogError(error.GenerateErrorReport());
                                    HandleFailed();
                                });
                        }
                        else if (keysToDelete.Any())
                        {
                            var keyToDelete = keysToDelete.First();

                            // Delete from cloud somehow
                            PlayFabClientAPI.ExecuteCloudScript(new ExecuteCloudScriptRequest
                            {
                                FunctionName = "setTitleData",
                                FunctionParameter = new
                                {
                                    key = keyToDelete.Key,
                                    data = (string)null,
                                    pw = "3Pj+GExYpTv_mK_*"
                                }
                            },
                                results =>
                                {
                                    Debug.Log(keyToDelete.Key + " deleted from Title Data!");

                                    keysToDelete.Remove(keyToDelete);

                                    SendData();
                                },
                                error =>
                                {
                                    Debug.LogError(error.GenerateErrorReport());

                                    keysToDelete.Remove(keyToDelete);
                                    failedKeys.Add(keyToDelete.Key);

                                    SendData();
                                });
                        }
                        else
                            returned.OnNext(failedKeys);
                    }
                }, error =>
                {
                    Debug.LogError(error.GenerateErrorReport());
                    returned.OnNext(new HashSet<string>(backlog.Keys));
                });
#else
            Observable.NextFrame()
                .Subscribe(_ => returned.OnNext(new HashSet<string>(backlog.Keys)));
#endif

            return returned;
        }
    }
}