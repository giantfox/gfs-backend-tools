﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using System;
using UniRx;
using PlayFab.ClientModels;
using Zenject;
using System.Linq;

namespace GFS.BackendTools
{
    public interface IUserDataController
    {
        IReadOnlyReactiveProperty<string> GetData(string key);
        void SetData(Dictionary<string, string> data);
    }

    public class UserDataBackendCache : BackendCache, IUserDataController
    {
        protected override string DiskKey => "UserData";

        protected override IObservable<HashSet<string>> SendData(Dictionary<string, string> backlog)
        {
            var returned = new Subject<HashSet<string>>();

            PlayFabClientAPI.UpdateUserData(new UpdateUserDataRequest
            {
                Data = backlog
                    .Where(kvp => !string.IsNullOrEmpty(kvp.Value))
                    .ToDictionary(kvp => kvp.Key, kvp => kvp.Value),
                KeysToRemove = backlog
                    .Where(kvp => string.IsNullOrEmpty(kvp.Value))
                    .Select(kvp => kvp.Key)
                    .ToList(),
            }, results => returned.OnNext(new HashSet<string>()),
            error =>
            {
                Debug.LogError(error.GenerateErrorReport());
                returned.OnNext(new HashSet<string>(backlog.Keys));
            });

            return returned;
        }

        protected override IObservable<Dictionary<string, string>> RefreshCache()
        {
            var returned = new Subject<Dictionary<string, string>>();

            PlayFabClientAPI.GetUserData(new GetUserDataRequest(),
                results =>
                {
                    returned.OnNext(results.Data
                        .ToDictionary(kvp => kvp.Key, kvp => kvp.Value.Value));
                }, error =>
                 {
                     Debug.LogError(error.GenerateErrorReport());
                     returned.OnNext(new Dictionary<string, string>());
                 });

            return returned;
        }

        IReadOnlyReactiveProperty<string> IUserDataController.GetData(string key)
        {
            return GetData(key);
        }

        void IUserDataController.SetData(Dictionary<string, string> data)
        {
            SetData(data);
        }
    }
}