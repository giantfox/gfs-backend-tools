﻿using System;
using System.Collections;
using System.Collections.Generic;
using PlayFab;
using PlayFab.ClientModels;
using UniRx;
using UnityEngine;
using System.Linq;

namespace GFS.BackendTools
{
    public interface ICharacterProfileController
    {
        IReadOnlyReactiveProperty<string[]> CharacterIds { get; }
        string CreateCharacter();
        IObservable<string> LocalIdToRemoteId(string localId);
        IObservable<string> RemoteIdTLocalId(string remoteId);
        Dictionary<string, string> CharactersData ();
        void DeleteCharacter ( string characterId, string characterKey, ICharacterRespositoryProvider<StringContainer> repository );
    }

    public class CharacterProfileBackendCache : BackendCache, ICharacterProfileController
    {
        public IReadOnlyReactiveProperty<string[]> CharacterIds => keys;

        Dictionary<string, string> charactersData;

        public Dictionary<string, string> CharactersData ()
        {
            return charactersData;
        }

        protected override string DiskKey => "CharacterProfiles";

        public string CreateCharacter()
        {
            var newLocalId = Guid.NewGuid().ToString().RemoveAllNonAlphaNumeric();
            SetData(new Dictionary<string, string> { { newLocalId, string.Empty } });
            return newLocalId;
        }

        public IObservable<string> LocalIdToRemoteId(string localId)
        {
            var returned = new Subject<string>();

            Observable.EveryUpdate()
                .First(_ => cache.ContainsKey(localId) && !string.IsNullOrEmpty(cache[localId].Value))
                .Subscribe(_ => returned.OnNext(cache[localId].Value));

            return returned;
        }

        public IObservable<string> RemoteIdTLocalId(string remoteId)
        {
            var returned = new Subject<string>();

            Observable.EveryUpdate()
                .First(_ => cache.Any(kvp => kvp.Value.Value == remoteId) && !string.IsNullOrEmpty(cache.First(kvp => kvp.Value.Value == remoteId).Value.Value))
                .Subscribe(_ => returned.OnNext(cache.First(kvp => kvp.Value.Value == remoteId).Key));

            return returned;
        }
        
        protected override IObservable<Dictionary<string, string>> RefreshCache ()
        {
            var returned = new Subject<Dictionary<string, string>>();
            var data = new Dictionary<string, string>();

            PlayFabClientAPI.GetAllUsersCharacters(new ListUsersCharactersRequest(),
                result =>
                {
                    var characters = result.Characters;

                    charactersData = new Dictionary<string, string> ();

                    Debug.Log ( "refreshing cache" );

                    foreach ( CharacterResult characterResult in characters )
                    {
                        Debug.Log ( characterResult.CharacterId );

                        charactersData.Add ( characterResult.CharacterName, characterResult.CharacterId );
                    }

                    returned.OnNext(characters
                        .ToDictionary(character => character.CharacterName, character => character.CharacterId));
                }, error => Debug.LogError(error));

            return returned;
        }

        protected override IObservable<HashSet<string>> SendData(Dictionary<string, string> backlog)
        {
            var returned = new Subject<HashSet<string>>();
            var failedKeys = new HashSet<string>();

            PlayFabClientAPI.GetAllUsersCharacters(new ListUsersCharactersRequest(),
               result =>
               {
                   var charactersToCreate = backlog
                        .Where(c => !result.Characters
                            .Any(p => p.CharacterName == c.Key))
                        .ToList();

                   void UpdateCharacter()
                   {
                       if (charactersToCreate.Any())
                       {
                           Debug.Log("Creating character!");
                           var characterToCreate = charactersToCreate.First();

                           Debug.Log ( "ff: " + characterToCreate.Key );

                           PlayFabClientAPI.GrantCharacterToUser(new GrantCharacterToUserRequest
                           {
                               CharacterName = characterToCreate.Key,
                               ItemId = "DefaultCharacter"
                           }, results =>
                           {
                               charactersToCreate.Remove(characterToCreate);
                               cache[characterToCreate.Key].Value = results.CharacterId;

                               Debug.Log ( characterToCreate.Key );

                               UpdateCharacter();

                               RefreshCache ();
                           }, error =>
                           {
                               Debug.LogError(error.GenerateErrorReport());

                               charactersToCreate.Remove(characterToCreate);
                               failedKeys.Add(characterToCreate.Key);

                               UpdateCharacter();
                           });
                       }
                       else
                       {
                           returned.OnNext ( failedKeys );
                       }
                   }

                   UpdateCharacter();
               }, error => Debug.LogError(error));

            return returned;
        }

        public void DeleteCharacter ( string characterId, string characterKey, ICharacterRespositoryProvider<StringContainer> repository )
        {
            Debug.Log ( "deleting character with id: " + characterId );

            ExecuteCloudScriptRequest executeCloudScript = new ExecuteCloudScriptRequest
            {
                FunctionName = "DeleteCharacter",
                FunctionParameter = new
                {
                    CharacterId = characterId
                }
            };
            PlayFabClientAPI.ExecuteCloudScript ( executeCloudScript,
            ( ExecuteCloudScriptResult result ) =>
            {
                Debug.Log ( "Character Deleted. Refreshing list now." );

                if ( result.FunctionResult != null ) Debug.Log ( result.FunctionResult.ToString () );

                cache[ characterKey ].Value = null;
                cache[ characterKey ] = null;

                bool returned = cache.Remove ( characterKey );

                Debug.Log ( returned );

                Debug.Log ( characterKey );

                repository.DeleteKey ( characterKey );

                RefreshCache ();
            },
            ( PlayFabError error ) =>
            {
                Debug.Log ( error.Error.ToString () );
            } );
        }
    }
}