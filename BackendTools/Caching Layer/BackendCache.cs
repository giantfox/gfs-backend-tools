﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GFS.Utilities;
using UniRx;
using UniRx.Diagnostics;
using UnityEngine;
using Zenject;

namespace GFS.BackendTools
{
    public abstract class ReadOnlyBackendCache : IInitializable
    {
        [Inject] protected ClientState clientState;

        protected ReactiveDictionary<string, ReactiveProperty<string>> cache { get; private set; } = new ReactiveDictionary<string, ReactiveProperty<string>>();
        protected ReactiveProperty<string[]> keys { get; private set; } = new ReactiveProperty<string[]>(new string[0]);
        protected HashSet<string> backlog = new HashSet<string>();

        protected abstract string DiskKey { get; }
        protected abstract IObservable<Dictionary<string, string>> RefreshCache();

        protected bool isDirty;
        private const bool encrypt = true;

        protected ReadOnlyBackendCache()
        {
            LoadCacheFromDisk();
        }

        public virtual void Initialize()
        {
            clientState.isLoggedIn
                .CombineLatest(clientState.isConnected, (isLoggedIn, isConnected) => isLoggedIn && isConnected)
                .DistinctUntilChanged()
                .Where(isLoggedIn => isLoggedIn)
                .Subscribe(_ => LaunchRefresh());

            cache.ObserveCountChanged()
                .Subscribe(_ => keys.Value = cache.Keys.ToArray());

            Observable.EveryEndOfFrame()
                .Where(_ => isDirty)
                .Subscribe(_ => SaveCacheToDisk());

            //clientState.onLogout
            //    .Subscribe(_ => ClearCache());

            keys.Value = cache.Keys.ToArray();
        }

        protected IReadOnlyReactiveProperty<string> GetData(string key)
        {
            if (!cache.ContainsKey(key))
                cache[key] = new ReactiveProperty<string>();
            return cache[key];
        }

        protected void LaunchRefresh ()
        {
            //ClearCache ();

            RefreshCache ()
               .First()
               .Subscribe(newCache =>
               {
                   foreach (var k in newCache.Keys)
                   {
                       if (!cache.ContainsKey(k))
                           cache[k] = new ReactiveProperty<string>();
                       cache[k].Value = newCache[k];
                   }

                   isDirty = true;
               });
        }

        protected void SaveCacheToDisk()
        {
            var json = JsonUtility.ToJson(new CacheJsonWrapper
            {
                data = cache
                    .Where(kvp =>
                    {
                        var isDeleted = string.IsNullOrEmpty(kvp.Value.Value);
                        var isBacklogged = backlog.Contains(kvp.Key);

                        return !isDeleted || isBacklogged;
                    })
                    .Select(kvp => new KVP
                    {
                        Key = kvp.Key,
                        Value = kvp.Value.Value
                    })
                    .ToList(),
                backlog = backlog.ToList()
            });

            if (encrypt)
                json = CryptoUtilities.EncryptStringAES(json, SystemInfo.deviceUniqueIdentifier);

            PlayerPrefs.SetString(DiskKey, json);
            PlayerPrefs.Save();

            isDirty = false;
        }

        protected void ClearCache()
        {
            foreach (var kvp in cache)
            {
                kvp.Value.Value = string.Empty;
            }

            backlog.Clear();

            PlayerPrefs.DeleteKey(DiskKey);
            PlayerPrefs.Save();
        }

        private void LoadCacheFromDisk()
        {
            var json = PlayerPrefs.GetString(DiskKey);
            if (string.IsNullOrEmpty(json))
                return;
            if (encrypt)
                json = CryptoUtilities.DecryptStringAES(json, SystemInfo.deviceUniqueIdentifier);

            var newCache = JsonUtility.FromJson<CacheJsonWrapper>(json);
            foreach (var kvp in newCache.data)
            {
                if (!cache.ContainsKey(kvp.Key))
                    cache.Add(kvp.Key, new ReactiveProperty<string>());

                cache[kvp.Key].Value = kvp.Value;
            }
            backlog = new HashSet<string>(newCache.backlog);
        }

        [Serializable]
        private class KVP
        {
            public string Key;
            public string Value;
        }

        [Serializable]
        private class CacheJsonWrapper
        {
            public List<KVP> data;
            public List<string> backlog;
        }
    }

    public abstract class BackendCache : ReadOnlyBackendCache
    {
        protected abstract IObservable<HashSet<string>> SendData(Dictionary<string, string> backlog);

        public override void Initialize()
        {
            base.Initialize();

            clientState.isConnected
                .CombineLatest(clientState.isLoggedIn, (isConnected, isLoggedIn) => isConnected && isLoggedIn)
                .DistinctUntilChanged()
                .Where(isConnected => isConnected && backlog.Any())
                .Subscribe(_ =>
                    SendData(cache
                        .Where(kvp => backlog.Contains(kvp.Key))
                        .ToDictionary(kvp => kvp.Key, kvp => kvp.Value.Value))
                    .First()
                    .Subscribe(newBacklog => backlog = newBacklog));
        }

        protected void SetData(Dictionary<string, string> data)
        {
            foreach (var kvp in data)
            {
                if (!cache.ContainsKey(kvp.Key))
                    cache[kvp.Key] = new ReactiveProperty<string>();
                cache[kvp.Key].Value = kvp.Value;
            }

            if (clientState.isConnected.Value)
                SendData(data)
                    .First()
                    .Subscribe(newBacklog => backlog = newBacklog);
            else
                foreach (var kvp in data)
                {
                    backlog.Add(kvp.Key);
                }

            isDirty = true;
        }
    }
}