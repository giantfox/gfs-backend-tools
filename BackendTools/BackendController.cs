﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.GroupsModels;
using SimpleJSON;
using System.Text;
using System.Text.RegularExpressions;
//using UniRx;
//using GFS.BackendTools;
using System.Linq;
using System.IO;
using EasyMobile;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;
using static GFS.BackendTools.GooglePlayPurchasesStrategy;

namespace GFS.Backend
{
    [System.Serializable]
    public class StoreItemData
    {
        public string id;
        public bool isPurchased;
        public SubscriptionData subscriptionData;
        public List<string> bundleItemIDs;
        public string localizedPrice;
    }

    [System.Serializable]
    public struct SubscriptionData
    {
        public bool isSubscription;
        public string expirationData;
        public string localizedIntroductoryPrice;
    }

    public class BackendController : MonoBehaviour
    {
        ///                                                                      ///
        ///                                                                      ///
        /// ******************************************************************** ///
        ///                                                                      ///
        ///                             P U B L I C                              ///

        #region PUBLIC

        public static BackendController instance;

        public string username;
        public string email = "d23@d.d";
        public string password = "123abc";

        public string masterUsername => _masterUsername;
        public string loggedInUsername =>
            personalData != null &&
            personalData[ "displayName" ] != null ?
            personalData[ "displayName" ].ToString ().Replace ("\"", "") : "";
        public string masterPlayfabID => _masterPlayFabID;

        public bool loggedInAsMaster => _loggedInAsMaster;
        public bool hasUserLoggedIn => entityID != "" || studentEntityID != "";
        public bool isMaster => personalData != null &&
                                personalData[ "isMaster" ] != null &&
                                personalData[ "isMaster" ].AsBool;
        // If you're a student, this boolean will always returns true because
        // students have madeup emails that obviously doesn't need to be
        // verified.
        public bool emailVerified => personalData[ "isMaster" ].AsBool ?
                                     _emailVerified : true;
        public bool emailLinked => _emailLinked;
        public bool isGuest => _isGuest;
        public bool isOffline;
        public bool hasOnlineAccount => PlayerPrefs.GetInt ("loggedOnlineOnce", 0) > 0 ? true : false;

        //public JSONArray levelsWords => _levelsWords;
        //public JSONObject playerGameData => _playerGameData;
        public JSONObject profilesData => _profilesData;

        public event System.Action<string> PurchaseComplete;
        public event System.Action<string> PurchaseFailed;

        public event System.Action<string> RestorePurchasesComplete;
        public event System.Action<string> RestorePurchasesFailed;

        public List<StoreItemData> storeItems;

        public string previousPurchasedItem = "";

        #endregion

        ///                                                                      ///
        ///                                                                      ///
        /// ******************************************************************** ///
        ///                                                                      ///
        ///                            P R I V A T E                             ///

        #region PRIVATE

        string entityID = "";
        string entityType;
        string _masterPlayFabID;
        string studentEntityID = "";
        string studentEmail = "";
        string studentPassword = "";
        string studentPlayfabID;
        string _masterUsername;
        bool hadPreviousStudentLoggedIn;
        //string currentPlayfabID = "";
        string localPath;

        bool _loggedInAsMaster = true;
        bool _emailVerified;
        bool _emailLinked;
        bool _isGuest;

        JSONObject personalData; // displayName, is Master or not, etc
        JSONObject groupData;

        JSONObject _profilesData; // students data like login, password
        //JSONArray _levelsWords;
        JSONObject _playerGameData; // level reached, words found, etc

        PlayFab.GroupsModels.EntityKey entityKey;
        PlayFab.GroupsModels.EntityKey groupKey;

        #endregion

        ///                                                                      ///
        ///                                                                      ///
        /// ******************************************************************** ///
        ///                                                                      ///
        ///                             S T A T I C                              ///

        #region STATIC

        static string groupPrefix = "profiles";
        static string[] chars = new string[ 26 ] { "A", "B", "C", "D", "E", "F", "G", "H",
        "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
        "W", "X", "Y", "Z" };

        #endregion

        ///                                                                      ///
        ///                                                                      ///
        /// ******************************************************************** ///
        ///                                                                      ///
        ///                     M O N O B E H A V I O U R                        ///

        #region MONOBEHAVIOUR

        //string playerDataFromPrefsBase64;

        private void Awake ()
        {
            instance = this;

            localPath = Application.persistentDataPath + "/playerGameData.json";

            //playerDataFromPrefsBase64 = PlayerPrefs.GetString ( "playerDataFromPrefsBase64" + currentPlayfabID, "" );

            DontDestroyOnLoad (this.gameObject);

            if ( !RuntimeManager.IsInitialized () )
                RuntimeManager.Init ();
        }

        void OnEnable ()
        {
            InAppPurchasing.PurchaseCompleted += PurchaseCompletedHandler;
            InAppPurchasing.PurchaseFailed += PurchaseFailedHandler;

            InAppPurchasing.RestoreCompleted += RestoreCompletedHandler;
            InAppPurchasing.RestoreFailed += RestoreFailedHandler;
        }

        // Unsubscribe when the game object is disabled
        void OnDisable ()
        {
            InAppPurchasing.PurchaseCompleted -= PurchaseCompletedHandler;
            InAppPurchasing.PurchaseFailed -= PurchaseFailedHandler;

            InAppPurchasing.RestoreCompleted -= RestoreCompletedHandler;
            InAppPurchasing.RestoreFailed -= RestoreFailedHandler;
        }

        #endregion

        ///                                                                      ///
        ///                                                                      ///
        /// ******************************************************************** ///
        ///                                                                      ///
        ///                      S I G N U P / L O G I N                         ///

        #region SIGNUP/LOGIN

        public void LoginAsGuest (System.Action success, System.Action<string, string> errorAction)
        {
            print ("Loggin in as a Guest");

            // If we're testing in the Editor, we are going to iOS here
            if ( Application.platform == RuntimePlatform.Android )
            {
                LoginWithAndroidDevice (() =>
                {
                    PlayerPrefs.SetInt ("loggedOnlineOnce", 1);

                    isOffline = false;

                    success?.Invoke ();
                    //LoginFlow ( result, success, errorAction, true );
                },
                (loginCode, loginError) =>
                {
                    SetOfflineBasedOnErrorCode (loginCode);

                    errorAction?.Invoke (loginCode, loginError);
                });
            }
            else
            {
                LoginWithIOSDevice (() =>
                {
                    PlayerPrefs.SetInt ("loggedOnlineOnce", 1);

                    isOffline = false;

                    success?.Invoke ();
                    //Login ( success, errorAction, true );
                },
                (loginCode, loginError) =>
                {
                    SetOfflineBasedOnErrorCode (loginCode);

                    errorAction?.Invoke (loginCode, loginError);
                });
            }
        }

        void LoginWithAndroidDevice (System.Action success, System.Action<string, string> errorAction)
        {
            PlayFabClientAPI.LoginWithAndroidDeviceID (new LoginWithAndroidDeviceIDRequest
            {
                CreateAccount = true,
                AndroidDeviceId = SystemInfo.deviceUniqueIdentifier,
                AndroidDevice = SystemInfo.deviceModel
            },
            result =>
            {
                isOffline = false;

                _isGuest = true;

                //currentPlayfabID = result.PlayFabId;
                PlayerPrefs.SetString ("lastLoggedInID", result.PlayFabId);

                if ( result.NewlyCreated )
                    RegistrationFlow (result.EntityToken.Entity, () =>
                    {
                        LoginFlow (result, success, errorAction);
                    }, errorAction);
                else
                    LoginFlow (result, success, errorAction);
            },
            error =>
            {
                SetOfflineBasedOnErrorCode (error.Error.ToString ());

                errorAction?.Invoke (error.Error.ToString (), error.GenerateErrorReport ());
            });
        }

        void LoginWithIOSDevice (System.Action success, System.Action<string, string> errorAction)
        {
            // For testing pourposes in Unity Editor
            string debugDeviceID = GetDebugDeviceID ();

            //if ( !IsPlayerLoggedIn () )
            //{
            PlayFabClientAPI.LoginWithIOSDeviceID (new LoginWithIOSDeviceIDRequest
            {
                CreateAccount = true,
                DeviceId = Application.platform ==
                RuntimePlatform.IPhonePlayer ?
                    SystemInfo.deviceUniqueIdentifier : debugDeviceID,
                DeviceModel = SystemInfo.deviceModel
            },
            result =>
            {
                isOffline = false;

                _isGuest = true;

                //currentPlayfabID = result.PlayFabId;
                PlayerPrefs.SetString ("lastLoggedInID", result.PlayFabId);

                if ( result.NewlyCreated )
                    RegistrationFlow (result.EntityToken.Entity, () =>
                    {
                        LoginFlow (result, success, errorAction);
                    }, errorAction);
                else
                    LoginFlow (result, success, errorAction);
            },
            error =>
            {
                SetOfflineBasedOnErrorCode (error.Error.ToString ());

                errorAction?.Invoke (error.Error.ToString (), error.GenerateErrorReport ());
            });

            //void Check ( LoginResult result = null )
            //{
            //    // means we got from playfab with login information
            //    if ( result != null )
            //    {
            //        _isGuest = true;

            //        if ( result.NewlyCreated )
            //            RegistrationFlow (result.EntityToken.Entity, () =>
            //            {
            //                LoginFlow (result, success, errorAction);
            //            }, errorAction);
            //        else
            //            LoginFlow (result, success, errorAction);
            //    }
            //    else
            //    {

            //    }
            //}
            //}
            //else if ( HasLocalPlayer () )
            //{

            //}
            //else
            //{

            //}
        }

        bool HasLocalPlayer ()
        {
            if ( personalData != null ) return true;
            else return false;
        }

        public void Register (System.Action success, System.Action<string, string> errorAction)
        {
            RegisterPlayfabUser (
                () =>
                {
                    PlayFabClientAPI.AddOrUpdateContactEmail (new AddOrUpdateContactEmailRequest
                    {
                        EmailAddress = email
                    },
                        _ =>
                        {
                            Login (() =>
                            {
                                PlayerPrefs.SetInt ("loggedOnlineOnce", 1);

                                isOffline = false;

                                success?.Invoke ();

                            },
                            (loginCode, loginError) =>
                            {
                                SetOfflineBasedOnErrorCode (loginCode);

                                errorAction?.Invoke (loginCode, loginError);
                            }, true);
                        },
                        addEmailError =>
                        {
                            SetOfflineBasedOnErrorCode (addEmailError.Error.ToString ());

                            errorAction?.Invoke (addEmailError.Error.ToString (), addEmailError.GenerateErrorReport ());
                        });
                },
                (errorCode, error) =>
                {
                    SetOfflineBasedOnErrorCode (errorCode);

                    errorAction?.Invoke (errorCode, error);
                });
        }

        void RegisterPlayfabUser (System.Action success, System.Action<string, string> errorAction)
        {
            // Register master playfab user
            PlayFabClientAPI.RegisterPlayFabUser (new RegisterPlayFabUserRequest ()
            {
                Email = email,
                Password = password,
                RequireBothUsernameAndEmail = false
            }, result =>
            {
                _isGuest = false;

                RegistrationFlow (result.EntityToken.Entity, success, errorAction);

            }, RegisterPlayFabUserError =>
            {
                print (RegisterPlayFabUserError.GenerateErrorReport ());

                errorAction?.Invoke (RegisterPlayFabUserError.Error.ToString (),
                    RegisterPlayFabUserError.GenerateErrorReport ());
            });
        }

        void LoginFlow (LoginResult result, System.Action success, System.Action<string, string> errorAction)
        {
            print ("LoginFlow");

            GetLoginData (result.PlayFabId,
                () =>
                {
                    studentEntityID = hadPreviousStudentLoggedIn ? "" : studentEntityID;

                    if ( personalData[ "isMaster" ] != null && personalData[ "isMaster" ].AsBool )
                        FillMasterData (result, () =>
                        {
                            print (studentPlayfabID);
                            print (personalData[ "loggedInAsStudent" ]);

                            // if we're returning from a logged in student, we
                            // should save that we're master now.
                            if ( hadPreviousStudentLoggedIn )
                            {
                                hadPreviousStudentLoggedIn = false;

                                SaveLoggedStudent ("", success, errorAction);
                            }
                            else
                            {
                                // there is a student login saved and we are not
                                // creating a new student, let's change to that
                                // account
                                if ( ( studentPlayfabID == "" || studentPlayfabID == null ) &&
                                personalData[ "loggedInAsStudent" ] != null &&
                                personalData[ "loggedInAsStudent" ] != "" )
                                {
                                    print ("loggin in as a student saved from master");

                                    LoginAsStudent (personalData[ "loggedInAsStudent" ], success, errorAction);
                                }
                                else
                                {
                                    success?.Invoke ();
                                }
                            }

                        }, errorAction);
                    else
                        FillStudentData (result, success, errorAction);

                },
                (errorCode, errorMSG) =>
                {
                    Debug.Log (errorMSG);

                    errorAction?.Invoke (errorCode, errorMSG);
                }
            );
        }

        void RegistrationFlow (PlayFab.ClientModels.EntityKey _entityKey, System.Action success, System.Action<string, string> errorAction)
        {
            personalData = new JSONObject ();
            personalData.Add ("isMaster", true);
            personalData.Add ("displayName", "user1"); // That's the name used in Frederick's orignal code

            PlayFabClientAPI.UpdateUserData (new UpdateUserDataRequest
            {
                Data = new Dictionary<string, string>
                {
                    {  "personal", personalData.ToString ()  }
                },
                Permission = UserDataPermission.Public

            }, updateUserDataResult =>
            {
                entityID = _entityKey.Id;
                entityType = _entityKey.Type;
                entityKey = new PlayFab.GroupsModels.EntityKey { Id = entityID, Type = entityType };

                LinkAccountToDevice (() =>
                {
                    StartCoroutine (CreateGroupOfProfiles (success, errorAction));
                },
                (errorCode, linkAccountToDeviceError) =>
                {
                    errorAction?.Invoke (errorCode, linkAccountToDeviceError);
                });

            }, UpdateUserDataError =>
            {
                print (UpdateUserDataError.GenerateErrorReport ());

                errorAction?.Invoke (UpdateUserDataError.Error.ToString (), UpdateUserDataError.GenerateErrorReport ());
            });
        }

        public void Login (System.Action sucess, System.Action<string, string> errorAction, bool shouldIgnoreBeenAGuest = false)
        {
            if ( shouldIgnoreBeenAGuest || !_isGuest )
            {
                print ("login in with email");

                LoginWithEmail (() =>
                {
                    PlayerPrefs.SetInt ("loggedOnlineOnce", 1);

                    isOffline = false;

                    sucess?.Invoke ();

                },
                (loginErrorCode, loginError) =>
                {
                    SetOfflineBasedOnErrorCode (loginErrorCode);

                    errorAction?.Invoke (loginErrorCode, loginError);
                });
            }
            else
            {
                print ("login in as guest");

                LoginAsGuest (() =>
                {
                    PlayerPrefs.SetInt ("loggedOnlineOnce", 1);

                    isOffline = false;

                    sucess?.Invoke ();

                },
                (loginErrorCode, loginError) =>
                {
                    SetOfflineBasedOnErrorCode (loginErrorCode);

                    errorAction?.Invoke (loginErrorCode, loginError);
                });
            }
        }

        void LoginWithEmail (System.Action sucess, System.Action<string, string> errorAction)
        {
            print ("email: " + email);
            print ("loggedinasmaster: " + _loggedInAsMaster);

            PlayFabClientAPI.LoginWithEmailAddress (new LoginWithEmailAddressRequest ()
            {
                Email = _loggedInAsMaster ? email : studentEmail,
                Password = _loggedInAsMaster ? password : studentPassword
            }, result =>
            {
                //currentPlayfabID = result.PlayFabId;
                PlayerPrefs.SetString ("lastLoggedInID", result.PlayFabId);

                LoginFlow (result, () =>
                {
                    if ( personalData[ "isMaster" ] != null && personalData[ "isMaster" ].AsBool )
                    {
                        LinkAccountToDevice (() =>
                        {
                            sucess?.Invoke ();
                        },
                        (errorCode, linkAccountToDeviceError) =>
                        {
                            errorAction?.Invoke (errorCode, linkAccountToDeviceError);
                        });
                    }
                    else
                    {
                        sucess?.Invoke ();
                    }

                }, errorAction);

            }, error =>
            {
                print (error.GenerateErrorReport ());

                errorAction?.Invoke (error.Error.ToString (), error.GenerateErrorReport ());
            });
        }

        void FillStudentData (LoginResult result, System.Action sucess, System.Action<string, string> errorAction)
        {
            print ("user is a student");

            _loggedInAsMaster = false;

            studentEntityID = result.EntityToken.Entity.Id;

            GetPlayerProfile (result.PlayFabId, () =>
            {
                GetPlayerGameData (result.PlayFabId, sucess, errorAction);
            },
            (errorCode, getPlayerProfileError) =>
            {
                errorAction?.Invoke (errorCode, getPlayerProfileError);
            });
        }

        void FillMasterData (LoginResult result, System.Action sucess, System.Action<string, string> errorAction)
        {
            print ("user is a master");

            // this check is important so I can know that the first account logged
            // is a master and any user after that is a student.
            _loggedInAsMaster = true;

            entityID = result.EntityToken.Entity.Id;
            entityType = result.EntityToken.Entity.Type;
            _masterPlayFabID = result.PlayFabId;
            entityKey = new PlayFab.GroupsModels.EntityKey { Id = entityID, Type = entityType };

            ListGroupMembers (() =>
            {
                GetAccuontInfo (_masterPlayFabID, () =>
                {
                    GetPlayerProfile (_masterPlayFabID, () =>
                    {
                        GetPlayerGameData (_masterPlayFabID, () =>
                        {
                            SaveNewProfile (sucess, errorAction);
                        },
                        (wordsDataErrorCode, wordsDataError) =>
                        {
                            errorAction?.Invoke (wordsDataErrorCode, wordsDataError);
                        });
                    },
                    (getPlayerProfileErrorCode, getPlayerProfileError) =>
                    {
                        errorAction?.Invoke (getPlayerProfileErrorCode, getPlayerProfileError);
                    });
                },
                (getAccountInfoErrorCode, getAccountInfoError) =>
                {
                    errorAction?.Invoke (getAccountInfoErrorCode, getAccountInfoError);
                });

            },
            (listGroupMembersErrorCode, listGroupMembersError) =>
            {
                errorAction?.Invoke (listGroupMembersErrorCode, listGroupMembersError);
            });
        }

        public void LoginAsStudent (string profileKey, System.Action sucess, System.Action<string, string> errorAction)
        {
            // before log in as a student, let's save which is the latest student
            // logged in, so next time we can log in directly as that student.
            SaveLoggedStudent (profileKey, () =>
            {
                hadPreviousStudentLoggedIn = true;

                studentEmail = profilesData[ profileKey ][ 0 ];
                studentPassword = profilesData[ profileKey ][ 1 ];
                studentPlayfabID = profileKey;

                _loggedInAsMaster = false;

                Login (() =>
                {
                    isOffline = false;

                    sucess?.Invoke ();

                },
                (loginErrorCode, loginError) =>
                {
                    SetOfflineBasedOnErrorCode (loginErrorCode);

                    errorAction?.Invoke (loginErrorCode, loginError);
                }, true);
            },
            (errorCode, error) =>
            {
                SetOfflineBasedOnErrorCode (errorCode);

                errorAction?.Invoke (errorCode, error);
            });
        }

        void SaveLoggedStudent (string profileKey, System.Action sucess, System.Action<string, string> errorAction)
        {
            if ( personalData[ "loggedInAsStudent" ] != null )
                personalData[ "loggedInAsStudent" ] = profileKey;
            else
                personalData.Add ("loggedInAsStudent", profileKey);

            PlayFabClientAPI.UpdateUserData (new UpdateUserDataRequest
            {
                Data = new Dictionary<string, string>
                {
                    {  "personal", personalData.ToString ()  }
                },
                Permission = UserDataPermission.Public

            }, updatesuerDataResult =>
            {
                print (JSON.Parse (updatesuerDataResult.ToJson ()));

                studentEntityID = "";

                _loggedInAsMaster = true;

                sucess?.Invoke ();

            },
            (error) =>
            {
                print (error.GenerateErrorReport ());

                errorAction?.Invoke (error.Error.ToString (), error.GenerateErrorReport ());
            });
        }

        public void LoginAsMaster (System.Action sucess, System.Action<string, string> errorAction)
        {
            print ("Login As Master");

            _loggedInAsMaster = true;

            Login (() =>
            {
                isOffline = false;

                sucess?.Invoke ();

            }, (loginErrorCode, loginError) =>
            {
                SetOfflineBasedOnErrorCode (loginErrorCode);

                errorAction?.Invoke (loginErrorCode, loginError);
            });
        }

        void LinkAccountToDevice (System.Action success, System.Action<string, string> errorAction)
        {
            // If we're testing in the Editor, we are going to iOS here
            if ( Application.platform == RuntimePlatform.Android )
            {
                LinkToAndroidDevice (success, errorAction);
            }
            else
            {
                LinkToIOSDevice (success, errorAction);
            }
        }

        void LinkToAndroidDevice (System.Action success, System.Action<string, string> errorAction)
        {
            PlayFabClientAPI.LinkAndroidDeviceID (new LinkAndroidDeviceIDRequest
            {
                ForceLink = true,
                AndroidDeviceId = SystemInfo.deviceUniqueIdentifier,
                AndroidDevice = SystemInfo.deviceModel
            },
            result =>
            {
                success?.Invoke ();
            },
            error =>
            {
                Debug.LogError (error.GenerateErrorReport ());

                errorAction?.Invoke (error.Error.ToString (), error.GenerateErrorReport ());
            });
        }

        void LinkToIOSDevice (System.Action success, System.Action<string, string> errorAction)
        {
            string debugDeviceID = GetDebugDeviceID ();

            PlayFabClientAPI.LinkIOSDeviceID (new LinkIOSDeviceIDRequest
            {
                ForceLink = true,
                DeviceId = Application.platform ==
                    RuntimePlatform.IPhonePlayer ?
                        SystemInfo.deviceUniqueIdentifier : debugDeviceID,
                DeviceModel = SystemInfo.deviceModel
            },
            result =>
            {
                print ("Linked to iOS Device");

                success?.Invoke ();
            },
            error =>
            {
                Debug.LogError (error.GenerateErrorReport ());

                errorAction?.Invoke (error.Error.ToString (), error.GenerateErrorReport ());
            });
        }

        public void LinkAnonymousAccountToEmail (System.Action sucess, System.Action<string, string> errorAction)
        {
            username = DefineUsername (email);

            if ( hasUserLoggedIn )
            {
                if ( !_emailLinked )
                {
                    PlayFabClientAPI.AddUsernamePassword (new AddUsernamePasswordRequest
                    {
                        Email = email,
                        Password = password,
                        Username = username
                    }, results =>
                    {
                        PlayFabClientAPI.AddOrUpdateContactEmail (new AddOrUpdateContactEmailRequest
                        {
                            EmailAddress = email
                        },
                        _ =>
                        {
                            Login (() =>
                            {
                                isOffline = false;

                                sucess?.Invoke ();
                            },
                            (loginErrorCode, loginError) =>
                            {
                                SetOfflineBasedOnErrorCode (loginErrorCode);

                                errorAction?.Invoke (loginErrorCode, loginError);

                            }, true);
                        },
                        addEmailError =>
                        {
                            SetOfflineBasedOnErrorCode (addEmailError.Error.ToString ());

                            errorAction?.Invoke (addEmailError.Error.ToString (), addEmailError.GenerateErrorReport ());
                        });

                    }, error =>
                    {
                        SetOfflineBasedOnErrorCode (error.Error.ToString ());

                        errorAction?.Invoke (error.Error.ToString (), error.GenerateErrorReport ());
                    });
                }
                else
                {
                    errorAction?.Invoke ("", "already has an email associated to this account");
                }
            }
            else
            {
                errorAction?.Invoke ("", "no user is logged in");
            }
        }

        public void Logout (System.Action sucess, System.Action<string, string> errorAction)
        {
            entityID = "";
            studentEntityID = "";
            email = "";
            password = "";

            PlayFabClientAPI.ForgetAllCredentials ();

            PlayerPrefs.SetInt ( "userIsLogged", 0 );

            sucess?.Invoke ();
        }

        #endregion

        ///                                                                      ///
        ///                                                                      ///
        /// ******************************************************************** ///
        ///                                                                      ///
        ///                           P R O F I L E S                            ///

        #region PROFILES

        IEnumerator CreateGroupOfProfiles (System.Action sucess, System.Action<string, string> errorAction)
        {
            yield return new WaitForSecondsRealtime (2f);

            PlayFabGroupsAPI.CreateGroup (new CreateGroupRequest
            {
                GroupName = groupPrefix + MadeUpGroupName (),
                Entity = entityKey
            }, response =>
            {
                Debug.Log ("Group Created: " + response.GroupName + " - " + response.Group.Id);

                //groupName = response.GroupName;
                groupKey = response.Group;

                StartCoroutine (StartRoutineToListNewlyCreatedGroup (sucess, errorAction));

            }, error =>
            {
                print (error.GenerateErrorReport ());
                print (error.Error);
                print (error.ErrorDetails);
                print (error.ErrorMessage);
            });
        }

        void GetLoginData (string playfabID, System.Action sucess, System.Action<string, string> errorAction)
        {
            PlayFabClientAPI.GetUserData (new GetUserDataRequest ()
            {
                PlayFabId = playfabID,
                Keys = null
            }, result =>
            {
                _profilesData = new JSONObject ();
                personalData = new JSONObject ();

                Debug.Log ("Got user data: ");

                foreach ( var key in result.Data.Keys )
                    Debug.Log (result.Data[ key ].Value);

                if ( result.Data == null || !result.Data.ContainsKey ("profiles") )
                {
                    Debug.Log ("No ProfilesData. Let's create one.");
                }
                else
                {
                    _profilesData = (JSONObject)JSON.Parse (result.Data[ "profiles" ].Value);
                }

                if ( result.Data != null && result.Data.ContainsKey ("personal") )
                {
                    personalData = JSON.Parse (result.Data[ "personal" ].Value).AsObject;
                }

                sucess?.Invoke ();

            }, error =>
            {
                errorAction?.Invoke (error.Error.ToString (), error.GenerateErrorReport ());
            });
        }

        void GetAccuontInfo (string playfabID, System.Action sucess, System.Action<string, string> errorAction)
        {
            PlayFabClientAPI.GetAccountInfo (new GetAccountInfoRequest
            {
                PlayFabId = playfabID

            }, result =>
            {
                _masterUsername = loggedInUsername;

                print ("x :" + loggedInUsername);

                sucess?.Invoke ();

            }, error =>
            {
                print (error.GenerateErrorReport ());

                errorAction?.Invoke (error.Error.ToString (), error.GenerateErrorReport ());
            });
        }

        public void UpdatePlayerAccountInfo (System.Action sucess, System.Action<string, string> errorAction)
        {
            GetPlayerProfile (_loggedInAsMaster ? _masterPlayFabID : studentPlayfabID,
                sucess, errorAction);
        }

        void GetPlayerProfile (string playfabID, System.Action sucess, System.Action<string, string> errorAction)
        {
            PlayFabClientAPI.GetPlayerCombinedInfo (new GetPlayerCombinedInfoRequest
            {
                InfoRequestParameters = new GetPlayerCombinedInfoRequestParams
                {
                    GetUserAccountInfo = true,
                    GetPlayerProfile = true,
                    ProfileConstraints = new PlayerProfileViewConstraints
                    {
                        ShowContactEmailAddresses = true,

                    }
                }

            }, result =>
            {
                foreach ( ContactEmailInfoModel email in result.InfoResultPayload.PlayerProfile.ContactEmailAddresses )
                {
                    if ( email.VerificationStatus == EmailVerificationStatus.Confirmed )
                        _emailVerified = true;
                }

                if ( result.InfoResultPayload.PlayerProfile.ContactEmailAddresses != null )
                {
                    _emailLinked = result.InfoResultPayload.PlayerProfile.ContactEmailAddresses.Count > 0;
                }
                else
                {
                    _emailLinked = false;
                }

                sucess?.Invoke ();
            }, error =>
            {
                errorAction?.Invoke (error.Error.ToString (), error.GenerateErrorReport ());
            });
        }

        void SaveNewProfile (System.Action sucess, System.Action<string, string> errorAction)
        {
            print ("new profile created: " + studentEntityID);

            if ( studentEntityID != "" )
            {
                Debug.Log (groupKey);
                Debug.Log (groupKey.Id);
                Debug.Log (studentEntityID);

                PlayFabClientAPI.ExecuteCloudScript (new ExecuteCloudScriptRequest
                {
                    FunctionName = "AddPlayerToGroup",
                    FunctionParameter = new
                    {
                        groupId = groupKey.Id,
                        entityId = studentEntityID
                    }
                },
                result =>
                {
                    studentEntityID = "";

                    ListGroupMembers (() =>
                    {
                        SaveProfileLoginData (sucess, errorAction);
                    }, (listGroupErrorCode, listGroupError) =>
                    {
                        errorAction?.Invoke (listGroupErrorCode, listGroupError);
                    });

                }, error =>
                {
                    print (error.ErrorDetails);
                    print (error.ErrorMessage);
                    print (error.Error);
                    print (error.GenerateErrorReport ());

                    errorAction?.Invoke (error.Error.ToString (), error.GenerateErrorReport ());
                });
            }
            else
            {
                sucess?.Invoke ();
            }
        }

        void SaveProfileLoginData (System.Action sucess, System.Action<string, string> errorAction)
        {
            if ( username == "" ) sucess?.Invoke ();

            // adding new profile and profile data
            JSONArray userData = new JSONArray ();
            userData.Add (studentEmail);
            userData.Add (studentPassword);
            userData.Add (username);

            profilesData.Add (studentPlayfabID, userData);

            print ("eeeeeee EEEEE EEEEEEEEEEE EEEEEE");

            PlayFabClientAPI.UpdateUserData (new UpdateUserDataRequest
            {
                Data = new Dictionary<string, string>
            {
                {  "profiles", profilesData.ToString () }
            }

            }, updatesuerDataResult =>
            {
                print ("success. Let's log in back to the new profile account.");

                //LoginBackAsStudentProfile ( username );
                Login (sucess, errorAction);

            }, error =>
            {
                print (error.GenerateErrorReport ());

                errorAction?.Invoke (error.Error.ToString (), error.GenerateErrorReport ());
            });
        }

        public void CreateProfile (System.Action sucess, System.Action<string, string> errorAction)
        {
            studentEmail = MadeUpEmail ();
            studentPassword = MadeUpPassword ();

            PlayFabClientAPI.RegisterPlayFabUser (new RegisterPlayFabUserRequest ()
            {
                Email = studentEmail,
                Password = studentPassword,
                RequireBothUsernameAndEmail = false
            }, result =>
            {
                personalData = new JSONObject ();
                personalData.Add ("isMaster", false);
                personalData.Add ("displayName", username);
                personalData.Add ("masterID", _masterPlayFabID);

                PlayFabClientAPI.UpdateUserData (new UpdateUserDataRequest
                {
                    Data = new Dictionary<string, string>
                {
                    {  "personal", personalData.ToString ()  }
                },
                    Permission = UserDataPermission.Public

                }, updatesuerDataResult =>
                {
                    studentEntityID = result.EntityToken.Entity.Id;
                    studentPlayfabID = result.PlayFabId;

                    Login (() =>
                    {
                        isOffline = false;

                        sucess?.Invoke ();

                    }, (loginErrorCode, loginError) =>
                    {
                        SetOfflineBasedOnErrorCode (loginErrorCode);

                        errorAction?.Invoke (loginErrorCode, loginError);
                    });

                }, error =>
                {
                    SetOfflineBasedOnErrorCode (error.Error.ToString ());

                    print (error.GenerateErrorReport ());

                    errorAction?.Invoke (error.Error.ToString (), error.GenerateErrorReport ());
                });

            }, error =>
            {
                SetOfflineBasedOnErrorCode (error.Error.ToString ());

                print (error.GenerateErrorReport ());

                errorAction?.Invoke (error.Error.ToString (), error.GenerateErrorReport ());
            });
        }

        ///                                                                      ///
        /// ******************************************************************** ///
        ///                                                                      ///

        public void Rename (string profileKey, string newStudentNaming, System.Action sucess, System.Action<string, string> errorAction)
        {
            profilesData[ profileKey ][ 2 ] = newStudentNaming;

            PlayFabClientAPI.UpdateUserData (new UpdateUserDataRequest
            {
                Data = new Dictionary<string, string>
                {
                    {  "profiles", profilesData.ToString () }
                }

            }, updatesuerDataResult =>
            {
                PlayFabClientAPI.ExecuteCloudScript (new ExecuteCloudScriptRequest
                {
                    FunctionName = "RenameStudentOwnData",
                    FunctionParameter = new
                    {
                        studentID = profileKey,
                        newStudentName = newStudentNaming
                    }
                },
                result =>
                {
                    print ("success. Student renamed. " + result.FunctionResult.ToString ());
                    //print ( "success. Student renamed. " + JSON.Parse ( result.CustomData.ToString () ) );

                    isOffline = false;

                    sucess?.Invoke ();

                }, error =>
                {
                    print (error.ErrorDetails);
                    print (error.ErrorMessage);
                    print (error.Error);
                    print (error.GenerateErrorReport ());

                    SetOfflineBasedOnErrorCode (error.Error.ToString ());

                    errorAction?.Invoke (error.Error.ToString (), error.GenerateErrorReport ());
                });

            }, error =>
            {
                print (error.GenerateErrorReport ());

                SetOfflineBasedOnErrorCode (error.Error.ToString ());

                errorAction?.Invoke (error.Error.ToString (), error.GenerateErrorReport ());
            });
        }

        public void DeleteStudent (string profileKey, System.Action sucess, System.Action<string, string> errorAction)
        {
            profilesData.Remove (profileKey);

            PlayFabClientAPI.UpdateUserData (new UpdateUserDataRequest
            {
                Data = new Dictionary<string, string>
            {
                {  "profiles", profilesData.ToString () }
            }

            }, updatesuerDataResult =>
            {
                print ("success. Student deleted");

                sucess?.Invoke ();

            }, error =>
            {
                print (error.GenerateErrorReport ());

                errorAction?.Invoke (error.Error.ToString (), error.GenerateErrorReport ());
            });
        }

        public void RenameDisplayName (string newDisplayName, System.Action sucess, System.Action<string, string> errorAction)
        {
            personalData[ "displayName" ] = newDisplayName;

            PlayFabClientAPI.UpdateUserData (new UpdateUserDataRequest
            {
                Data = new Dictionary<string, string>
                {
                    {  "personal", personalData.ToString ()  }
                },
                Permission = UserDataPermission.Public

            }, updatesuerDataResult =>
            {
                isOffline = false;

                if ( isMaster )
                {
                    print ("success. Master changed his/her name.");

                    _masterUsername = loggedInUsername;
                }
                else
                {
                    print ("success. Student changed his/her name.");
                }

                sucess?.Invoke ();

            }, error =>
            {
                SetOfflineBasedOnErrorCode (error.Error.ToString ());

                print (error.GenerateErrorReport ());

                errorAction?.Invoke (error.Error.ToString (), error.GenerateErrorReport ());
            });
        }

        #endregion

        ///                                                                      ///
        ///                                                                      ///
        /// ******************************************************************** ///
        ///                                                                      ///
        ///                            G R O U P S                               ///

        #region GROUPS

        IEnumerator StartRoutineToListNewlyCreatedGroup (System.Action sucess, System.Action<string, string> errorAction)
        {
            yield return new WaitForSecondsRealtime (2f);

            ListGroupMembers (sucess, errorAction);
        }

        void ListGroupMembers (System.Action sucess, System.Action<string, string> errorAction)
        {
            Debug.Log (entityKey);

            groupData = new JSONObject ();

            PlayFabGroupsAPI.ListMembership (new ListMembershipRequest ()
            {
                Entity = entityKey
            }, result =>
            {
                Debug.Log (result.Groups.Count);

                foreach ( GroupWithRoles group in result.Groups )
                {
                    if ( group.GroupName.Contains (groupPrefix) )
                    {
                        print ("2 " + group.GroupName);

                        groupKey = group.Group;

                        PlayFabGroupsAPI.ListGroupMembers (new ListGroupMembersRequest
                        {
                            Group = group.Group
                        }, groupMembersResult =>
                        {
                            print ("3 " + groupMembersResult.Members.Count);

                            for ( int i = 0; i < groupMembersResult.Members.Count; i++ )
                            {
                                EntityMemberRole memberRole = groupMembersResult.Members[ i ];

                                if ( memberRole.Members.Count > 0 )
                                {
                                    groupData.Add (
                                        memberRole.Members[ 0 ].Key.Id,
                                        memberRole.Members[ 0 ].Lineage[ "master_player_account" ].Id);
                                }
                            }

                            sucess?.Invoke ();

                        }, error =>
                        {
                            print (error.GenerateErrorReport ());

                            errorAction?.Invoke (error.Error.ToString (), error.GenerateErrorReport ());
                        });
                    }
                }

            }, error =>
            {
                print (error.GenerateErrorReport ());

                errorAction?.Invoke (error.Error.ToString (), error.GenerateErrorReport ());
            });
        }

        #endregion

        ///                                                                      ///
        ///                                                                      ///
        /// ******************************************************************** ///
        ///                                                                      ///
        ///                             R E P O R T                              ///

        #region REPORT

        public void GetReport (System.Action sucess, System.Action<string, string> errorAction)
        {
            PlayFabClientAPI.ExecuteCloudScript (new ExecuteCloudScriptRequest
            {
                FunctionName = "GetListOfPlayersData",
                FunctionParameter = new
                {
                    groupId = groupKey.Id,
                    studentsRange = 0
                }
            },
            result =>
            {
                print (result.FunctionResult);

                isOffline = false;

                sucess?.Invoke ();

            }, error =>
            {
                print (error.ErrorDetails);
                print (error.ErrorMessage);
                print (error.Error);
                print (error.GenerateErrorReport ());

                SetOfflineBasedOnErrorCode (error.Error.ToString ());

                errorAction?.Invoke (error.Error.ToString (), error.GenerateErrorReport ());
            });
        }

        #endregion

        ///                                                                      ///
        ///                                                                      ///
        /// ******************************************************************** ///
        ///                                                                      ///
        ///                              W O R D S                               ///

        #region WORDS

        void CheckIfPlayerDataIsCreatedLocally ()
        {
            if ( _playerGameData == null )
            {
                string lastLoggedInID = PlayerPrefs.GetString ("lastLoggedInID", "");

                print (lastLoggedInID);

                string playerDataFromPrefsBase64 = PlayerPrefs.GetString ("playerDataFromPrefsBase64" + lastLoggedInID, "");

                if ( playerDataFromPrefsBase64 == "" )
                {
                    print ("entrou aqui?");

                    _playerGameData = new JSONObject ();

                    _playerGameData.Add ("gameData", new JSONArray ());
                    _playerGameData.Add ("version", 0);
                }
                else
                {
                    JSONObject _tempPlayerGameData = (JSONObject)JSONObject.LoadFromBinaryBase64 (playerDataFromPrefsBase64);

                    if ( isMaster )
                    {
                        string playerDataFromPrefsBase64MasterOldData = PlayerPrefs.GetString ("playerDataFromPrefsBase64" + "", "");

                        JSONObject _tempPlayerGameDataOldMaster = (JSONObject)JSONObject.
                            LoadFromBinaryBase64 (playerDataFromPrefsBase64MasterOldData);

                        if ( _tempPlayerGameDataOldMaster[ "version" ].AsInt > _tempPlayerGameData[ "version" ].AsInt )
                        {
                            _tempPlayerGameData = _tempPlayerGameDataOldMaster;
                        }
                    }

                    _playerGameData = _tempPlayerGameData;
                }
            }
        }

        public JSONObject GetPlayerGameData ()
        {
            CheckIfPlayerDataIsCreatedLocally ();

            return _playerGameData;
        }

        public void AddNewData (string value, string key)
        {
            CheckIfPlayerDataIsCreatedLocally ();

            _playerGameData[ value ] = key;
        }

        void GetPlayerGameData (string playfabID, System.Action sucess, System.Action<string, string> errorAction)
        {
            PlayFabClientAPI.GetUserData (new GetUserDataRequest ()
            {
                PlayFabId = playfabID,
                Keys = null
            }, result =>
            {
                _playerGameData = null;

                CheckIfPlayerDataIsCreatedLocally ();

                JSONObject oldPlayerData = _playerGameData;

                //_playerGameData = new JSONObject ();

                if ( result.Data == null || !result.Data.ContainsKey ("gameData") )
                {
                    Debug.Log ("No game data. Let's create one if we don't have any locally.");

                    //_playerGameData = null;

                    //CheckIfPlayerDataIsCreatedLocally ();

                    //if ( oldPlayerData != null )
                    //{
                    //    if ( oldPlayerData[ "playfabID" ] == null ||
                    //    oldPlayerData[ "playfabID" ].Value == playfabID )
                    //    {
                    //        _playerGameData = oldPlayerData;
                    //    }
                    //    else
                    //    {
                    //        CheckIfPlayerDataIsCreatedLocally ();
                    //    }
                    //}
                    //else
                    //{
                    //    CheckIfPlayerDataIsCreatedLocally ();
                    //}
                    _playerGameData = oldPlayerData;
                }
                else
                {
                    _playerGameData = JSON.Parse (result.Data[ "gameData" ].Value).AsObject;

                    //if 
                    //if ( _playerGameData[ "version" ] < oldPlayerData [ "version" ] )
                    if ( oldPlayerData != null )
                    {
                        //if ( oldPlayerData[ "playfabID" ].Value == playfabID )
                        //{
                        if ( _playerGameData[ "version" ].AsInt < oldPlayerData[ "version" ].AsInt )
                            _playerGameData = oldPlayerData;
                        //}
                    }
                }

                sucess?.Invoke ();

            }, error =>
            {
                Debug.Log (error.GenerateErrorReport ());

                errorAction.Invoke (error.Error.ToString (), error.GenerateErrorReport ());
            });
        }

        public void SavePlayerGameData (System.Action sucess, System.Action<string, string> errorAction)
        {
            CheckIfPlayerDataIsCreatedLocally ();

            _playerGameData[ "version" ] = _playerGameData[ "version" ].AsInt + 1;

            if ( !hasUserLoggedIn )
            {
                isOffline = true;

                SaveLocallyIfOffline (sucess);

                errorAction?.Invoke ("", "no player loaded yet");
                return;
            }

            print ("gggggggg");
            print (_playerGameData);
            print (hasUserLoggedIn);
            print (instance);
            print (_playerGameData.ToString ());

            //_playerGameData[ "playfabID" ] = currentPlayfabID;

            PlayFabClientAPI.UpdateUserData (new UpdateUserDataRequest
            {
                Data = new Dictionary<string, string>
            {
                {  "gameData", _playerGameData.ToString () }
            },
                Permission = UserDataPermission.Public

            }, updatesuerDataResult =>
            {
                print ("game data saved succefully, in playfab and locally, for the current logged in player!");

                isOffline = false;

                SaveLocallyIfOffline (sucess);

            }, error =>
            {
                print (error.GenerateErrorReport ());

                SetOfflineBasedOnErrorCode (error.Error.ToString ());

                SaveLocallyIfOffline (sucess);

                errorAction?.Invoke (error.Error.ToString (), error.GenerateErrorReport ());
            });
        }

        void SaveLocallyIfOffline (System.Action sucess)
        {
            //if ( isOffline )
            //{
            string lastLoggedInID = PlayerPrefs.GetString ("lastLoggedInID", "");

            string playerDataFromPrefsBase64 = _playerGameData.SaveToBinaryBase64 ();

            //JSONObject _tempPlayerGameData = (JSONObject)JSONObject.LoadFromBinaryBase64 (playerDataFromPrefsBase64);

            //if ( isMaster )
            //{
            //    string playerDataFromPrefsBase64MasterOldData = PlayerPrefs.GetString ("playerDataFromPrefsBase64" + "", "");

            //    JSONObject _tempPlayerGameDataOldMaster = (JSONObject)JSONObject.
            //        LoadFromBinaryBase64 (playerDataFromPrefsBase64MasterOldData);

            //    if ( _tempPlayerGameDataOldMaster[ "version" ].AsInt > _tempPlayerGameData[ "version" ].AsInt )
            //    {
            //        _tempPlayerGameData = _tempPlayerGameDataOldMaster;
            //    }
            //}

            //_playerGameData = _tempPlayerGameData;
            print (lastLoggedInID);

            PlayerPrefs.SetString ("playerDataFromPrefsBase64" + lastLoggedInID, playerDataFromPrefsBase64);

            print ("game data saved succefully locally!");

            sucess?.Invoke ();
            //}
        }

        JSONArray LevelsWords ()
        {
            JSONArray levelsWords = new JSONArray ();

            // level 1
            JSONArray wordsPerLevel = new JSONArray ();
            wordsPerLevel.Add ("2");
            wordsPerLevel.Add ("4");
            wordsPerLevel.Add ("0");

            levelsWords.Add (wordsPerLevel);

            wordsPerLevel = new JSONArray ();
            wordsPerLevel.Add ("1");
            wordsPerLevel.Add ("2");
            wordsPerLevel.Add ("5");

            levelsWords.Add (wordsPerLevel);

            wordsPerLevel = new JSONArray ();
            wordsPerLevel.Add ("3");
            wordsPerLevel.Add ("6");
            wordsPerLevel.Add ("7");
            wordsPerLevel.Add ("8");

            levelsWords.Add (wordsPerLevel);

            wordsPerLevel = new JSONArray ();
            wordsPerLevel.Add ("0");
            wordsPerLevel.Add ("3");
            wordsPerLevel.Add ("4");
            wordsPerLevel.Add ("5");

            levelsWords.Add (wordsPerLevel);

            return levelsWords;
        }

        //public void MigrateOldData (ReactiveDictionary<string, ReactiveProperty<string>> cache,
        //    ICharacterRespositoryProvider<StringContainer> characterRepoProvider,
        //    ICharacterProfileController characterProfileBackendCache,
        //    System.Action sucess, System.Action<string, string> errorAction)
        //{
        //    StartCoroutine (MigrateData (cache, characterRepoProvider, characterProfileBackendCache,
        //        sucess, errorAction));
        //}

        //IEnumerator MigrateData (ReactiveDictionary<string, ReactiveProperty<string>> cache,
        //    ICharacterRespositoryProvider<StringContainer> characterRepoProvider,
        //    ICharacterProfileController characterProfileBackendCache,
        //    System.Action sucess, System.Action<string, string> errorAction)
        //{
        //    Dictionary<string, string> dic = cache
        //        //.Where ( kvp => backlog.Contains ( kvp.Key ) )
        //        .ToDictionary (kvp => kvp.Key, kvp => kvp.Value.Value);

        //    Debug.Log ("----1---- " + characterProfileBackendCache.CharacterIds.Value.Length);
        //    Debug.Log ("----1---- " + cache.Keys.Count);

        //    Dictionary<string, Dictionary<string, string>> oldCache = new Dictionary<string, Dictionary<string, string>> ();

        //    foreach ( string v in cache.Keys )
        //    {
        //        //Debug.Log ( v + " " + cache[v] );

        //        JSONNode keyNode = SimpleJSON.JSON.Parse (v);
        //        JSONNode valueNode = SimpleJSON.JSON.Parse (cache[ v ].Value);

        //        string characterId = SimpleJSON.JSON.Parse (v)[ "characterId" ];
        //        string key = SimpleJSON.JSON.Parse (v)[ "key" ];
        //        Debug.Log (characterId);
        //        Debug.Log (key);

        //        if ( valueNode != null && valueNode[ "data" ] != null )
        //        {
        //            if ( !oldCache.Keys.Contains (characterId) )
        //            {
        //                oldCache.Add (characterId, new Dictionary<string, string> ());
        //            }

        //            Dictionary<string, string> keys = oldCache[ characterId ];

        //            keys.Add (key, valueNode[ "data" ]);
        //        }
        //    }

        //    //Debug.Log ( "sss: " + characterRepoProvider.CharacterIds ().Count );

        //    foreach ( string key in oldCache.Keys )
        //    {
        //        var newCharacterId = characterProfileBackendCache.CreateCharacter ();
        //        var currentCharacterRepo = characterRepoProvider.GetCharacterRespository (newCharacterId);

        //        Dictionary<string, string> characterKeys = oldCache[ key ];

        //        foreach ( string valueKey in characterKeys.Keys )
        //        {
        //            currentCharacterRepo.SetCharacterData (valueKey, new StringContainer { data = characterKeys[ valueKey ] });
        //        }

        //        currentCharacterRepo.SaveCharacterData ();

        //        yield return new WaitForSecondsRealtime (1.5f);
        //    }

        //    //foreach ( var c in characterRepoProvider.CharacterIds () )
        //    //{
        //    //    //if ( characterProfileBackendCache.CharactersData () != null )
        //    //    //{
        //    //    //Debug.Log ( c + " " + characterProfileBackendCache.CharactersData ()[ c ] );
        //    //    var characterDisplayName = characterRepoProvider.GetCharacterRespository ( c ).GetData ( "Name" ).Value?.data;

        //    //    Debug.Log ( characterDisplayName );

        //    //    var newCharacterId = characterProfileBackendCache.CreateCharacter ();

        //    //    var currentCharacterRepo = characterRepoProvider.GetCharacterRespository ( newCharacterId );

        //    //    Debug.Log ( "FFFFFFFFFFFFFFFFFFF " + currentCharacterRepo.Keys.Value.Count );

        //    //    foreach ( var k in currentCharacterRepo.Keys.Value )
        //    //    {
        //    //        var displayedValue = currentCharacterRepo.GetData ( k ).Value?.data;

        //    //        Debug.Log ( displayedValue );

        //    //        currentCharacterRepo.SetCharacterData ( k, new StringContainer { data = displayedValue } );

        //    //        //currentCharacterRepo.SetData (
        //    //        //    "Name",
        //    //        //    new StringContainer { data = characterDisplayName },
        //    //        //    new StringContainer { data = authenticationController.deviceId } );
        //    //    }

        //    //    currentCharacterRepo.SaveCharacterData ();

        //    //    yield return new WaitForSecondsRealtime ( 1.5f );
        //    //}

        //    Debug.Log ("--------");

        //    yield return new WaitForSecondsRealtime (1.5f);

        //    //RefreshData ();

        //    yield return new WaitForSecondsRealtime (3);

        //    foreach ( var c in characterProfileBackendCache.CharacterIds.Value )
        //    {
        //        var character = characterRepoProvider.GetCharacterRespository (c);

        //        character.SaveCharacterData ();
        //    }
        //}

        #endregion


        ///                                                                      ///
        ///                                                                      ///
        /// ******************************************************************** ///
        ///                                                                      ///
        ///                          P U R C H A S E                             ///

        #region purchases

        void SaveUnregisteredReceipt (string name, string isoCurrencyCode, string localizedPrice,
            string receipt, string signature)
        {
            print ("Saving receipts to local unregistered list");

            string receipts = PlayerPrefs.GetString ("receipts", "");

            JSONArray receiptsData = new JSONArray ();

            if ( receipts != "" )
            {
                receiptsData = (JSONArray)JSONObject.LoadFromBinaryBase64 (receipts);
            }

            JSONObject newReceiptData = new JSONObject ();
            newReceiptData[ "name" ] = name;
            newReceiptData[ "isoCurrencyCode" ] = isoCurrencyCode;
            newReceiptData[ "localizedPrice" ] = localizedPrice;
            newReceiptData[ "receipt" ] = receipt;
            newReceiptData[ "signature" ] = signature;
            newReceiptData[ "saved" ] = false;

            bool canAdd = true;

            for ( int i = 0; i < receiptsData.Count; i++ )
            {
                JSONObject receiptData = (JSONObject)receiptsData[ i ];

                if ( receiptData[ "name" ].Value == newReceiptData[ "name" ].Value )
                {
                    receiptData[ "receipt" ] = newReceiptData[ "receipt" ].Value;

                    canAdd = false;
                }
            }

            if ( canAdd )
                receiptsData.Add (newReceiptData);

            receipts = receiptsData.SaveToBinaryBase64 ();
            PlayerPrefs.SetString ("receipts", receipts);

            print ("Saved. Now it has " + receiptsData.Count + " receipts to be saved.");
        }

        void SaveReceiptAtIndex (int index)
        {
            string receipts = PlayerPrefs.GetString ("receipts", "");

            JSONArray receiptsData = (JSONArray)JSONObject.LoadFromBinaryBase64 (receipts);

            //for ( int i = 0; i < receiptsData.Count; i++ )
            //{
            JSONObject receiptData = (JSONObject)receiptsData[ index ];

            print (receiptData[ "name" ]);
            print (receiptData[ "isoCurrencyCode" ]);
            print (receiptData[ "localizedPrice" ]);
            print (receiptData[ "receipt" ]);
            print (receiptData[ "signature" ]);
            print (receiptData[ "saved" ]);

            receiptData[ "saved" ] = true;
            //}

            receipts = receiptsData.SaveToBinaryBase64 ();
            PlayerPrefs.SetString ("receipts", receipts);

            print ("Saved. Now it has " + receiptsData.Count + " receipts to be saved.");
        }

        (JSONObject, int) NextReceiptThatNeedsToBeSavedInPlayfab ()
        {
            string receipts = PlayerPrefs.GetString ("receipts", "");

            JSONArray receiptsData = (JSONArray)JSONObject.LoadFromBinaryBase64 (receipts);

            for ( int i = 0; i < receiptsData.Count; i++ )
            {
                JSONObject receiptData = (JSONObject)receiptsData[ i ];

                // it means we have something to be saved
                if ( !receiptData[ "saved" ].AsBool )
                    return (receiptData, i);
            }

            return (null, 0);
        }

        public void SaveReceiptsToPlayfab (System.Action sucess, System.Action<string, string> errorAction)
        {
            (JSONObject, int) receiptDataToBeSavedAtPlayfab = NextReceiptThatNeedsToBeSavedInPlayfab ();

            if ( receiptDataToBeSavedAtPlayfab.Item1 == null )
            {
                print ("no receipt to be saved at playfab");

                sucess?.Invoke ();

                return;
            }
            else
            {
                decimal price = System.Convert.ToDecimal (receiptDataToBeSavedAtPlayfab.Item1[ "localizedPrice" ].Value);

                if ( Application.platform == RuntimePlatform.IPhonePlayer )
                {
                    // Invoke receipt validation
                    // This will not only validate a receipt, but will also grant player corresponding items
                    // only if receipt is valid.
                    PlayFabClientAPI.ValidateIOSReceipt (new ValidateIOSReceiptRequest ()
                    {
                        // Pass in currency code in ISO format
                        CurrencyCode = receiptDataToBeSavedAtPlayfab.Item1[ "isoCurrencyCode" ],
                        // Convert and set Purchase price
                        PurchasePrice = (int)( price * 100 ),
                        ReceiptData = receiptDataToBeSavedAtPlayfab.Item1[ "receipt" ]
                    },
                    result =>
                    {
                        print ("Saved succefully to playfab index: " + receiptDataToBeSavedAtPlayfab.Item2);

                        SaveReceiptAtIndex (receiptDataToBeSavedAtPlayfab.Item2);

                        SaveReceiptsToPlayfab (sucess, errorAction);
                    },
                    error =>
                    {
                        if ( error.Error.ToString () == "ReceiptAlreadyUsed" ||
                        error.Error.ToString () == "SubscriptionAlreadyTaken" )
                        {
                            print ("Receipt already saved at playfab");

                            SaveReceiptAtIndex (receiptDataToBeSavedAtPlayfab.Item2);

                            SaveReceiptsToPlayfab (sucess, errorAction);
                        }
                        else
                        {
                            print ("Error at saving receipts at playfab. " + error.GenerateErrorReport ());

                            errorAction?.Invoke (error.Error.ToString (), error.GenerateErrorReport ());
                        }
                    }
                    );
                }
                //else
                //{
                //    PlayFabClientAPI.ValidateGooglePlayPurchase (new ValidateGooglePlayPurchaseRequest ()
                //    {
                //        // Pass in currency code in ISO format
                //        CurrencyCode = e.purchasedProduct.metadata.isoCurrencyCode,
                //        // Convert and set Purchase price
                //        PurchasePrice = (uint)( e.purchasedProduct.metadata.localizedPrice * 100 ),
                //        // Pass in the receipt
                //        ReceiptJson = googleReceipt.payloadData.json,
                //        // Pass in the signature
                //        Signature = googleReceipt.payloadData.signature
                //    }, result =>
                //    {
                //        returned.OnNext (true);
                //    },
                //   error =>
                //   {
                //       Debug.LogError (error.GenerateErrorReport ());
                //       returned.OnNext (false);
                //   }
                //);
                //}
            }



            //string receipts = PlayerPrefs.GetString ("receipts", "");

            //JSONArray receiptsData = (JSONArray)JSONObject.LoadFromBinaryBase64 ( receipts );

            //print ( "it has " + receiptsData.Count + " in the list of receipts." );

            //Debug.Log ("xxxxxxxx xxxxxxxx xxxxxxxx xxxxxxxx xxxxxxxx xxxxxxxx");

            //for ( int i = 0; i < receiptsData.Count; i++ )
            //{
            //    JSONObject receiptData = (JSONObject)receiptsData[ i ];

            //print (receiptData[ "name" ] );
            //print (receiptData[ "isoCurrencyCode" ]);
            //print (receiptData[ "localizedPrice" ]);
            //print (receiptData[ "receipt" ]);
            //print (receiptData[ "signature" ]);
            //print (receiptData[ "saved" ]);

            //SaveReceiptAtIndex ( i );
            //}

        }

        public void Purchase (string productName)
        {
            InAppPurchasing.Purchase (productName);
        }

        void PurchaseCompletedHandler (IAPProduct product)
        {
            Debug.Log ("--------------");
            Debug.Log ("1- purcahased last item: " + PlayerPrefs.GetString ("lastPUrchasedItem"));

            previousPurchasedItem = product.Name;


            if ( product.Name == EM_IAPConstants.Product_unlock_game ||
                product.Name == EM_IAPConstants.Product_unlock_25off ||
                product.Name == EM_IAPConstants.Product_unlock_50off )
            {
                PlayerPrefs.SetString ("lastPUrchasedItem", "unlock");

                PlayerPrefs.SetInt ("playerGotUnlockGame", 1);

                PurchaseComplete?.Invoke (product.Name);
            }
            else if ( product.Name == EM_IAPConstants.Product_com_englishanyone_frederick_unlockgamesubscription ||
                product.Name == EM_IAPConstants.Product_com_englishanyone_frederick_unlockgamesubscriptionfreemonth )
            {
                PlayerPrefs.SetString ("lastPUrchasedItem", "subscription");

                PlayerPrefs.SetInt ("playerGotFredFamilySubscriptionBefore", 1);

                PurchaseComplete?.Invoke (product.Name);
            }
            else if ( product.Name == EM_IAPConstants.Product_com_englishanyone_frederick_subscriptionandlevelpackbundle ||
                product.Name == EM_IAPConstants.Product_com_englishanyone_frederick_subscriptionandlevelpackbundlediscount )
            {
                PurchaseComplete?.Invoke (product.Name);
            }

            Debug.Log ("Product name: " + product.Name);

            string productName = product.Name;
            string isoCurrencyCode = "";
            string localizedPrice = "";
            string receipt = "";
            string signature = "";

#if EM_UIAP
            Product sampleProduct = InAppPurchasing.GetProduct (product.Name);

            if ( sampleProduct != null )
            {
                if ( Application.platform == RuntimePlatform.IPhonePlayer )
                {
                    if ( sampleProduct.hasReceipt )
                    {
                        Debug.Log (sampleProduct.receipt);

                        Debug.Log ("fffffff");

                        Debug.Log (JSON.Parse (sampleProduct.receipt));

                        Debug.Log ("Receipt: " + JSON.Parse (sampleProduct.receipt)[ "Payload" ].Value);

                        Debug.Log ("Receipt2: " + ( (JSONObject)JSON.Parse (sampleProduct.receipt) )[ "Payload" ].Value);

                        receipt = JSON.Parse (sampleProduct.receipt)[ "Payload" ].Value;
                    }
                    else
                    {
                        print ("Doesn't have receipt");
                    }
                }

                if ( Application.platform == RuntimePlatform.Android )
                {
                    if ( sampleProduct.hasReceipt )
                    {
                        var googleReceipt = GooglePurchase.FromJson (sampleProduct.receipt);

                        Debug.Log ("Receipt: " + googleReceipt.payloadData.json);

                        receipt = googleReceipt.payloadData.json;
                        signature = googleReceipt.payloadData.signature;
                    }
                    else
                    {
                        print ("Doesn't have receipt");
                    }
                }

                ProductMetadata locMetaData = InAppPurchasing.GetProductLocalizedData (product.Name);

                if ( locMetaData != null )
                {
                    Debug.Log ("Localized price: " + locMetaData.localizedPrice);
                    Debug.Log ("iso currency code: " + locMetaData.isoCurrencyCode);

                    isoCurrencyCode = locMetaData.isoCurrencyCode.ToString ();
                    localizedPrice = locMetaData.localizedPrice.ToString ();
                }

                if ( product.Type == IAPProductType.Subscription )
                {
                    // Get the subscription information of the current product,
                    // note that this method takes the product name as input.
                    SubscriptionInfo info = InAppPurchasing.GetSubscriptionInfo (product.Name);

                    if ( info == null )
                    {
                        Debug.Log ("The subscription information of this product could not be retrieved.");
                    }

                    Debug.Log ("Expire Date: " + info.getExpireDate ());
                }
            }
#endif

            SaveUnregisteredReceipt (productName, isoCurrencyCode, localizedPrice, receipt, signature);
        }

        public bool IsPlayerElegibleForPromotion ()
        {
            string lastPurchasedItem = PlayerPrefs.GetString ("lastPUrchasedItem", "");

            Debug.Log ("2- purcahased last item: " + PlayerPrefs.GetString ("lastPUrchasedItem"));

            if ( lastPurchasedItem == "subscription" )
            {
                int hasUnlockGame = PlayerPrefs.GetInt ("playerGotUnlockGame", 0);

                if ( hasUnlockGame == 0 )
                    return true;
                else
                {
                    return false;
                }
            }
            else if ( lastPurchasedItem == "unlock" )
            {
                int hadSubscription = PlayerPrefs.GetInt ("playerGotFredFamilySubscriptionBefore", 0);

                if ( hadSubscription == 0 )
                    return true;
                else
                {
                    return false;
                }
            }
            else
                return false;
        }

        public string GetPromotionItemID ()
        {
            string lastPurchasedItem = PlayerPrefs.GetString ("lastPUrchasedItem", "");

            PlayerPrefs.SetString ("lastPUrchasedItem", "");

            if ( lastPurchasedItem == "subscription" )
            {
                return "unlock_50off";
            }
            else if ( lastPurchasedItem == "unlock" )
            {
                return "com.englishanyone.frederick.unlockgamesubscriptionfreemonth";
            }
            else
            {
                return "";
            }
        }

        // Failed purchase handler
        void PurchaseFailedHandler (IAPProduct product, string failureReason)
        {
            PurchaseFailed?.Invoke (product.Name);

            Debug.Log ("The purchase of product " + product.Name + " has failed with reason: " + failureReason);
        }

        public void RestorePurchases ()
        {
            InAppPurchasing.RestorePurchases ();
        }

        void RestoreCompletedHandler ()
        {
            Debug.Log ("All purchases have been restored successfully.");

            RestorePurchasesComplete?.Invoke ("");
        }

        // Failed restoration handler
        void RestoreFailedHandler ()
        {
            Debug.Log ("The purchase restoration has failed.");

            RestorePurchasesFailed?.Invoke ("");
        }

        public void GetAllProductsInformation ()
        {
            IAPProduct[] products = InAppPurchasing.GetAllIAPProducts ();

            // Print all product names
            foreach ( IAPProduct prod in products )
            {
                Debug.Log ("----------");

                Debug.Log ("Product name: " + prod.Name);

#if EM_UIAP
                Product sampleProduct = InAppPurchasing.GetProduct (prod.Name);

                if ( sampleProduct != null )
                {
                    Debug.Log ("Available To Purchase: " + sampleProduct.availableToPurchase.ToString ());
                    if ( sampleProduct.hasReceipt )
                    {
                        Debug.Log ("Receipt: " + JSON.Parse (sampleProduct.receipt)[ "Payload" ].Value);
                    }

                    if ( Application.platform == RuntimePlatform.IPhonePlayer )
                    {
                        // EM_IAPConstants.Sample_Product is the generated name constant of a product named "Sample Product".
                        AppleInAppPurchaseReceipt receipt = InAppPurchasing.GetAppleIAPReceipt (prod.Name);

                        // Print the receipt content.
                        if ( receipt != null )
                        {
                            Debug.Log ("Product ID: " + receipt.productID);
                            Debug.Log ("Original Purchase Date: " + receipt.originalPurchaseDate.ToShortDateString ());
                            Debug.Log ("Original Transaction ID: " + receipt.originalTransactionIdentifier);
                            Debug.Log ("Purchase Date: " + receipt.purchaseDate.ToShortDateString ());
                            Debug.Log ("Transaction ID: " + receipt.transactionID);
                            Debug.Log ("Quantity: " + receipt.quantity);
                            Debug.Log ("Cancellation Date: " + receipt.cancellationDate.ToShortDateString ());
                            Debug.Log ("Subscription Expiration Date: " + receipt.subscriptionExpirationDate.ToShortDateString ());
                        }
                    }

                    ProductMetadata locMetaData = InAppPurchasing.GetProductLocalizedData (prod.Name);

                    if ( locMetaData != null )
                    {
                        Debug.Log ("Localized title: " + locMetaData.localizedTitle);
                        Debug.Log ("Localized description: " + locMetaData.localizedDescription);
                        Debug.Log ("Localized price string: " + locMetaData.localizedPriceString);
                        Debug.Log ("Localized price: " + locMetaData.localizedPrice);
                        Debug.Log ("iso currency code: " + locMetaData.isoCurrencyCode);
                    }

                    if ( prod.Type == IAPProductType.Subscription )
                    {
                        // Get the subscription information of the current product,
                        // note that this method takes the product name as input.
                        SubscriptionInfo info = InAppPurchasing.GetSubscriptionInfo (prod.Name);

                        if ( info == null )
                        {
                            Debug.Log ("The subscription information of this product could not be retrieved.");
                            continue;
                        }

                        // Prints subscription info.
                        Debug.Log ("Product ID: " + info.getProductId ());
                        Debug.Log ("Purchase Date: " + info.getPurchaseDate ());
                        Debug.Log ("Is Subscribed: " + info.isSubscribed ());
                        Debug.Log ("Is Expired: " + info.isExpired ());
                        Debug.Log ("Is Cancelled: " + info.isCancelled ());
                        Debug.Log ("Is FreeTrial: " + info.isFreeTrial ());
                        Debug.Log ("Is Auto Renewing: " + info.isAutoRenewing ());
                        Debug.Log ("Remaining Time: " + info.getRemainingTime ().ToString ());
                        Debug.Log ("Is Introductory Price Period: " + info.isIntroductoryPricePeriod ());
                        Debug.Log ("Introductory Price Period: " + info.getIntroductoryPricePeriod ().ToString ());
                        Debug.Log ("Introductory Price Period Cycles: " + info.getIntroductoryPricePeriodCycles ());
                        Debug.Log ("Introductory Price: " + info.getIntroductoryPrice ());
                        Debug.Log ("Expire Date: " + info.getExpireDate ());
                    }
                }
#endif
            }
        }

        public void GetCatalogData (System.Action success, System.Action<string, string> errorAction)
        {
            // It is needed to clear here?
            previousPurchasedItem = "";

            if ( !PlayFabClientAPI.IsClientLoggedIn () )
            {
                print ( "Client is not logged in, getting catalog data locally." );

                StartCoroutine (WaitToLoadLocalCatalog (success, errorAction));

                
            }
            else
            {
                print ( "Client is logged in, getting catalog data from Playfab." );

                GetPlayfabCatalogData ( success, errorAction );
            }
        }

        IEnumerator WaitToLoadLocalCatalog ( System.Action success, System.Action<string, string> errorAction )
        {
            bool shouldContinue = true;

            while ( shouldContinue )
            {
                foreach ( GFS.Backend.StoreItemData storeItem in storeItems )
                {
                    Debug.Log (storeItem.id);

                    if ( storeItem.id.Contains ( "unlock.game" ) )
                    {
                        if ( storeItem.localizedPrice != null && storeItem.localizedPrice != "" )
                            shouldContinue = false;
                    }
                }

                yield return new WaitForSecondsRealtime (1.0f);

                GetLocalCatalogData ();
            }

            success?.Invoke ();
        }

        void GetLocalCatalogData ()
        {
            IAPProduct[] products = InAppPurchasing.GetAllIAPProducts ();

            storeItems = new List<StoreItemData> ();

            foreach ( IAPProduct prod in products )
            {

                string localizedPrice = "";

                bool isPurchased = false;

#if EM_UIAP

                ProductMetadata data = InAppPurchasing.GetProductLocalizedData (prod.Name);

                if ( data != null )
                {
                    Debug.Log ("Localized title: " + data.localizedTitle);
                    Debug.Log ("Localized description: " + data.localizedDescription);
                    Debug.Log ("Localized price string: " + data.localizedPriceString);
                    Debug.Log ( data.localizedPrice);

                    localizedPrice = data.localizedPriceString;
                }

                isPurchased = InAppPurchasing.IsProductOwned (prod);

                Debug.Log (isPurchased);
#endif

                StoreItemData storeItem = new StoreItemData
                {
                    id = prod.Name,
                    isPurchased = isPurchased,
                    subscriptionData = DefineSubscriptionData (prod),
                    localizedPrice = localizedPrice
                };

                VerifyExpirationDate ( storeItem );

                storeItems.Add (storeItem);
            }

            
        }

        void GetPlayfabCatalogData ( System.Action success, System.Action<string, string> errorAction )
        {
            UpdatePlayfabSubscriptionExpirationData (
                (long)( System.DateTime.UtcNow - new System.DateTime (1970, 1, 1) ).TotalMilliseconds,
                (string date) => { Debug.Log ("date: " + date); });

            PlayFabClientAPI.GetCatalogItems (new GetCatalogItemsRequest (), result =>
            {
                storeItems = new List<StoreItemData> ();

                List<CatalogItem> catalog = result.Catalog;

                // Register each item from the catalog
                foreach ( CatalogItem item in catalog )
                {
                    if ( item.Bundle != null )
                    {
                        Debug.Log (item.ItemId);

                        StoreItemData storeItem = new StoreItemData
                        {
                            id = item.ItemId,
                            isPurchased = false,
                            bundleItemIDs = item.Bundle.BundledItems
                        };

                        storeItems.Add (storeItem);
                    }
                }

                PlayFabClientAPI.GetUserInventory (new GetUserInventoryRequest (),
                    results =>
                    {
                        foreach ( ItemInstance userItem in results.Inventory )
                        {
                            foreach ( StoreItemData storeItem in storeItems )
                            {
                                storeItem.subscriptionData = GetItemSubscriptionData (storeItem.id);
                                    
                                foreach ( string bundleItemID in storeItem.bundleItemIDs )
                                {
                                    if ( bundleItemID == userItem.ItemId )
                                    {
                                        Debug.Log (bundleItemID);

                                        if ( storeItem.subscriptionData.isSubscription )
                                        {
                                            VerifyExpirationDate (storeItem);
                                        }
                                        else
                                        {
                                            storeItem.isPurchased = true;
                                        }
                                    }
                                }
                            }
                        }

                        success?.Invoke ();
                    },
                    getUserInventoryError =>
                    {
                        errorAction?.Invoke (getUserInventoryError.Error.ToString (),
                            getUserInventoryError.GenerateErrorReport ());
                    });

            }, error =>
            {
                //Debug.LogError (error.GenerateErrorReport ());
                errorAction?.Invoke (error.Error.ToString (), error.GenerateErrorReport ());
            });
        }
        
        void VerifyExpirationDate ( StoreItemData storeItem )
        {
            if ( storeItem.subscriptionData.expirationData == null ) return;

            System.DateTime subscriptionExpirationDate = System.DateTime.Parse (storeItem.subscriptionData.expirationData);

            var isMobileSubscritionActive = subscriptionExpirationDate >= System.DateTime.UtcNow.Date;

            long expirationDateInMilliseconds = (long)( subscriptionExpirationDate - new System.DateTime (1970, 1, 1) ).TotalMilliseconds;

            if ( !isMobileSubscritionActive )
            {
                if ( PlayFabClientAPI.IsClientLoggedIn () )
                {
                    UpdatePlayfabSubscriptionExpirationData (expirationDateInMilliseconds, (string date) =>
                    {
                        System.DateTime playfabExpirationDate = new System.DateTime (1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc)
                            .AddMilliseconds (System.Convert.ToDouble (date));

                        bool isPlayfabSubscriptionActive = playfabExpirationDate >= System.DateTime.UtcNow.Date;

                        if ( !isPlayfabSubscriptionActive )
                            storeItem.isPurchased = false;
                    });
                }
                else
                {
                    print ( "User is not logged in, so we can't verify the expiration date of this subscription" +
                        "in Playfab, only locally. Item: " + storeItem.id );

                    storeItem.isPurchased = false;
                }
            }
            else
            {
                if ( PlayFabClientAPI.IsClientLoggedIn () )
                {
                    UpdatePlayfabSubscriptionExpirationData (expirationDateInMilliseconds, (string date) => { });
                }

                storeItem.isPurchased = true;
            }
        }

        SubscriptionData GetItemSubscriptionData ( string itemID )
        {
            IAPProduct[] products = InAppPurchasing.GetAllIAPProducts ();

            // Print all product names
            foreach ( IAPProduct prod in products )
            {
                if ( prod.Name != itemID ) continue;

                return DefineSubscriptionData (prod);
            }

            return new SubscriptionData ();
        }

        SubscriptionData DefineSubscriptionData (IAPProduct item )
        {
#if EM_UIAP
            Product sampleProduct = InAppPurchasing.GetProduct (item.Name);

            if ( sampleProduct != null )
            {
                Debug.Log ("Available To Purchase: " + sampleProduct.availableToPurchase.ToString ());

                if ( item.Type == IAPProductType.Subscription )
                {
                    SubscriptionInfo info = InAppPurchasing.GetSubscriptionInfo (item.Name);

                    print ("TTTTTTTTTTTT TTTTTTTTT TTTTTTTTT TTTTTTTTT");

                    print ( item.Name );

                    if ( info == null )
                    {
                        Debug.Log ("The subscription information of this product could not be retrieved, but" +
                            "it is a subscription.");

                        return new SubscriptionData
                        {
                            isSubscription = true,
                            expirationData = new System.DateTime (1990, 01, 01).ToString ("yyyy-MM-dd")
                        };
                    }

                    // we still have time
                    if ( info.getRemainingTime ().TotalSeconds > 0 )
                    {
                        Debug.Log ("We are subscribed and still not expired");

                        return new SubscriptionData
                        {
                            isSubscription = true,
                            expirationData = info.getExpireDate ().ToString ("yyyy-MM-dd"),
                            localizedIntroductoryPrice = info.getIntroductoryPrice ()
                        };
                    }
                    else
                    {
                        Debug.Log ("we never had this subscription or it's expired. Let's compare with " +
                            "Playfab now to make sure which option is the right.");

                        return new SubscriptionData
                        {
                            isSubscription = true,
                            expirationData = new System.DateTime (1990, 01, 01).ToString ("yyyy-MM-dd"),
                            localizedIntroductoryPrice = info.getIntroductoryPrice ()
                        };
                    }
                }
            }
#endif

            return new SubscriptionData ();
        }

        void UpdatePlayfabSubscriptionExpirationData (long expirationDate, System.Action<string> action)
        {
            Debug.Log ("called UpdatePlayfabSubscriptionExpirationData");

            ExecuteCloudScriptRequest executeCloudScript = new ExecuteCloudScriptRequest
            {
                FunctionName = "UpdateSubscriptionData",
                FunctionParameter = new
                {
                    EXPIRATIONDATE = expirationDate
                }
            };
            PlayFabClientAPI.ExecuteCloudScript (executeCloudScript,
            (ExecuteCloudScriptResult result) =>
            {
                Debug.Log ("Subscription Date: " + result.FunctionResult.ToString ());

                Debug.Log (result.FunctionResult.ToString ());

                action?.Invoke (result.FunctionResult.ToString ());
            },
            (PlayFabError error) =>
            {
                Debug.Log (error.Error.ToString ());

                action?.Invoke ("");
            });
        }

#endregion

        ///                                                                      ///
        ///                                                                      ///
        /// ******************************************************************** ///
        ///                                                                      ///
        ///                           P R I V A T E                              ///

#region PRIVATE

        string GetDebugDeviceID ()
        {
            string debugDeviceID = PlayerPrefs.GetString ("debugDeviceID", "");

            if ( debugDeviceID == "" )
            {
                debugDeviceID = MadeUpDeviceID ();
                PlayerPrefs.SetString ("debugDeviceID", debugDeviceID);
            }

            return debugDeviceID;
        }

        string DefineUsername (string email)
        {
            string username = email?.RemoveAllNonAlphaNumeric ();

            if ( username.Length > 20 )
                username = email?.RemoveAllNonAlphaNumeric ().Substring (0,
                    email.RemoveAllNonAlphaNumeric ().Length >= 20 ? 20 : email.RemoveAllNonAlphaNumeric ().Length);

            return username;
        }

        string MadeUpEmail ()
        {
            string email = "";

            email += RandomChars (10);

            email += "@";

            email += RandomChars (5);

            email += ".";

            email += RandomChars (3);

            return email;
        }

        string MadeUpPassword ()
        {
            return RandomChars (6);
        }

        string MadeUpGroupName ()
        {
            return RandomChars (10);
        }

        string MadeUpDeviceID ()
        {
            return RandomChars (20);
        }

        string RandomChars (int length)
        {
            string randomChars = "";

            for ( int i = 0; i < length; i++ )
                randomChars += chars[ Random.Range (0, chars.Length) ];

            return randomChars;
        }

        bool IsPlayerLoggedIn ()
        {
            return !string.IsNullOrEmpty (PlayFabSettings.staticPlayer.ClientSessionTicket);
        }

        void SetOfflineBasedOnErrorCode (string code)
        {
            if ( code == "APIClientRequestRateLimitExceeded" ||
                code == "APIConcurrentRequestLimitExceeded" ||
                code == "ConcurrentEditError" ||
                code == "DataUpdateRateExceeded" ||
                code == "DownstreamServiceUnavailable" ||
                code == "InvalidAPIEndpoint" ||
                code == "OverLimit" ||
                code == "ServiceUnavailable" )
            {
                print ("isOffline");

                isOffline = true;
            }
        }

#endregion
    }

    public static class BackendUtils
    {
        public static string RemoveAllNonAlphaNumeric (this string str)
        {
            Regex rgx = new Regex ("[^a-zA-Z0-9 -]");
            return rgx.Replace (str, "");
        }
    }
}